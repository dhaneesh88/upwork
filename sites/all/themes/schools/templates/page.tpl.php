<?php include DRUPAL_ROOT . '/' . path_to_theme() . '/templates/inc/header.inc'; ?>
<?php if(drupal_is_front_page()) : ?>  
  <div class="index-sections">
  	<?php print render($page['front_sections']); ?>
  </div>
<?php else : ?>
  <?php print render($page['content']); ?>
<?php endif; ?>
<?php include DRUPAL_ROOT . '/' . path_to_theme() . '/templates/inc/footer.inc'; ?>