<!--Footer-->
    <div class="footer">
      <div class="email-row ">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="block-content">
                <h2 ><?php print theme_get_setting('footer_headline'); ?></h2>
                <p><?php print theme_get_setting('footer_subtitle'); ?></p>
                <div> </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-section">
                <form>
                  <input placeholder="Enter your email" class="form-control form-text" type="text" id="edit-email" name="email" value="" size="60" maxlength="128">
                  <button type="submit" id="edit-submit--2" name="op" value="Sign Up!" class="btn btn-default form-submit"><?php print theme_get_setting('footer_sign_up_cta'); ?></button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="link-row">
        <div class="goto-top smooth-scroll list-unstyled"><a class="scroll toparrow " href="#main-wrapper" ><!-- Go To Top --></a></div>
        <div class="container">
          <div class="row">
            <div class="col-md-8">
              <div class="links-top">
                <ul>
                  <li class="first leaf"><a href="<?php print theme_get_setting('footer_link_1_url'); ?>"><?php print theme_get_setting('footer_link_1_text'); ?></a></li> 
                  <li><a href="<?php print theme_get_setting('footer_link_2_url'); ?>"><?php print theme_get_setting('footer_link_2_text'); ?></a></li>
                  <li><a href="<?php print theme_get_setting('footer_link_3_url'); ?>"><?php print theme_get_setting('footer_link_3_text'); ?></a></li>
                  <li><a href="<?php print theme_get_setting('footer_link_4_url'); ?>"><?php print theme_get_setting('footer_link_4_text'); ?></a></li>
                  <li class=" "><a href="<?php print theme_get_setting('footer_link_5_url'); ?>"><?php print theme_get_setting('footer_link_5_text'); ?></a></li>
                </ul>
              </div>
              <div class="links-small">
                <ul>
                  <li ><a href="<?php print theme_get_setting('footer_link_2R_1_url'); ?> "><?php print theme_get_setting('footer_link_2R_1_text'); ?> </a></li> 
                  <li><a href="<?php print theme_get_setting('footer_link_2R_2_url'); ?>"><?php print theme_get_setting('footer_link_2R_2_text'); ?> </a></li>
                  <li><a href="<?php print theme_get_setting('footer_link_2R_3_url'); ?>"><?php print theme_get_setting('footer_link_2R_3_text'); ?> </a></li>
                  <li ><a href="<?php print theme_get_setting('footer_link_2R_4_url'); ?>"><?php print theme_get_setting('footer_link_2R_4_text'); ?> </a></li>
                  <li><a href="<?php print theme_get_setting('footer_link_2R_5_url'); ?>"><?php print theme_get_setting('footer_link_2R_5_text'); ?> </a></li>
                  <li ><a href="<?php print theme_get_setting('footer_link_2R_6_url'); ?>"><?php print theme_get_setting('footer_link_2R_6_text'); ?> </a></li>
                  <li><a href="<?php print theme_get_setting('footer_link_2R_7_url'); ?>"><?php print theme_get_setting('footer_link_2R_7_text'); ?> </a></li>
                </ul>
              </div>
              <div class="link-btns"> 
                <p> <a href="<?php print theme_get_setting('footer_button_1_url'); ?>" class="btn btn-md btn-white-border"><?php print theme_get_setting('footer_button_1'); ?></a> <a href="<?php print theme_get_setting('footer_button_2_url'); ?>" class="btn btn-md btn-white-border"><?php print theme_get_setting('footer_button_2'); ?></a> <span href="#" class="phone-no"> <span class="or">Or</span> <span class="icon icon-ki-faci-phone"></span> <span><?php print theme_get_setting('footer_phone_number'); ?></span> </span> </p>
              </div>
            </div>
            <div class="col-md-4">    
              <div class="social-icon-col">
                <ul >
                  <li><a href="#"> <span class="icon icon-ki-facebook"></span> </a></li>
                  <li class="leaf"><a href="#"> <span class="icon icon-ki-twitter"></span> </a></li>
                  <li class="leaf"><a href="#"> <span class="icon icon-ki-youtube"></span> </a></li>
                  <li class="leaf"><a href="#"> <span class="icon icon-ki-instagram"></span> </a></li>
                  <li class="last leaf"><a href="#"> <span class="icon icon-ki-snapchat"></span> </a></li>
                </ul>
              </div>
              <div class="blog-col"> <a href="//www.kaplaninternational.com/blog/" target="_blank" class="btn">Kaplan Blog</a> </div>
            </div>
          </div>
        </div>
      </div>
      <div class="logo-block">
        <div class="container">
          <h3 >Kaplan International English is accredited by</h3>
          <div class="row">
            <div class="col-md-2"> 
              <div class="block">
                <h3><?php print theme_get_setting('acc_1'); ?></h3>   
                <a href="<?php print theme_get_setting('acc_1_url'); ?>"> <img src="<?php print theme_get_setting('acc_1_image'); ?>" /> </a> </div>
            </div>
            <div class="col-md-2">
              <div class="block">
                <h3><?php print theme_get_setting('acc_2'); ?></h3>
                <a href="<?php print theme_get_setting('acc_2_url'); ?>"> <img src="<?php print theme_get_setting('acc_2_image'); ?>" width="89" height="62" /></a> </div>
            </div>
            <div class="col-md-2">
              <div class="block">
                <h3><?php print theme_get_setting('acc_3'); ?></h3>
                <a href="<?php print theme_get_setting('acc_3_url'); ?>"> <img src="<?php print theme_get_setting('acc_3_image'); ?>" /></a> </div>
            </div>
            <div class="col-md-2">
              <div class="block">
                <h3><?php print theme_get_setting('acc_4'); ?></h3>
                <a href="<?php print theme_get_setting('acc_4_url'); ?>"> <img src="<?php print theme_get_setting('acc_4_image'); ?>" /></a> </div>
            </div>
            <div class="col-md-2">
              <div class="block">
                <h3><?php print theme_get_setting('acc_5'); ?></h3>
                <a href="<?php print theme_get_setting('acc_5_url'); ?>"> <img src="<?php print theme_get_setting('acc_5_image'); ?>" /></a> </div>
            </div>
            <div class="col-md-2">
              <div class="block">
                <h3><?php print theme_get_setting('acc_6'); ?></h3>
                <a href="<?php print theme_get_setting('acc_6_url'); ?>"> <img src="<?php print theme_get_setting('acc_6_image'); ?>" width="37" height="60" /></a> </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyright">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 address-k">
              <p class="text-center">© 2018 Aspect Education Ltd, Reg No: 4053877 / VAT No: 152088224 / Reg office: 2F, Warwick Building, Kensington Village, Avonmore Road, London, W14 8HQ UK / <a href="http://www.miitbeian.gov.cn" target="blank">京ICP备09019829号</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  