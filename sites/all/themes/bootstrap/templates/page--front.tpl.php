<div class="content-section">
     <div class="banner-section-top">
      <div class="inner-banner">
         <div class="large-right">
           <div class="bg-img full-bg">
           <div class="bg-img-light full-bg">
              <div class="ks-logo"><img src="<?php print theme_get_setting('hero_logo'); ?>"/></div>
              <div class="banner-text">
                <h2><?php print theme_get_setting('hero_text'); ?></h2>
                <div class="tag-line"><?php print theme_get_setting('hero_subtitle'); ?></div>
                <div class="button-section">
                  <div class="btn-blue"><a class="btn btn-1 " href="hero_button_1_url"><?php print theme_get_setting('hero_button_1_text'); ?></a></div>
                  <div class="btn-blue"><a class="btn btn-2 " href="hero_button_2_url"><?php print theme_get_setting('hero_button_2_text'); ?></a></div>
                </div>
              </div>
              
           </div>
           </div>
         
         </div>
         <div class="img-cols right-banner">
            <div class="img-col-1 course">    
             <a href="<?php print theme_get_setting('right_box_1_url'); ?>" class="full-bg">
               <span class="full-block full-bg" >
               <span class="text-h-m "> 
                  <span class="text-h"><?php print theme_get_setting('right_box_1_caption'); ?></span><span class="text-more"><?php print theme_get_setting('right_box_1_roll_text'); ?> →</span></span>
               </span>
             </a>
            </div>
            <div class="img-col-1 all pos-rel ">
             <a href="<?php print theme_get_setting('right_box_2_url'); ?>" class="full-bg">
               <span class="full-block full-bg" >
               <span class="text-h-m "> 
                  <span class="text-h"><?php print theme_get_setting('right_box_2_caption'); ?></span><span class="text-more"><?php print theme_get_setting('right_box_2_roll_text'); ?> →</span></span>
               </span>
             </a>
             <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
                <div class="carousel-inner">
                     <div class="carousel-item item active one-img full-bg"></div>
                     <div class="carousel-item item two-img full-bg"></div>
                     <div class="carousel-item item three-img full-bg"></div>
                </div>
             </div>
            </div>
            
            
            <div class="img-col-1">
             <a href="<?php print theme_get_setting('right_box_3_url'); ?>" class="full-bg">
               <span class="full-block full-bg" >
               <span class="text-h-m "> 
                  <span class="text-h"><?php print theme_get_setting('right_box_3_caption'); ?></span><span class="text-more"><?php print theme_get_setting('right_box_3_roll_text'); ?> →</span></span>
               </span>
             </a>
            </div>
            
         </div>
      </div>
     
     </div>
     
     <div class="in-section-2">
        <div class="container">
       <div class="row">
       <div class="col-md-12">
                <h2> <?php print theme_get_setting('homepage_h2'); ?>
      <span><?php print theme_get_setting('homepage_subtitle'); ?></span></h2>
      </div>
       </div>
       <div class="row">
         <div class="col-sm-3 col-xs-6">
         <img src="<?php print theme_get_setting('home_box_1_icon'); ?>" />
         <p><?php print theme_get_setting('home_box_1_message'); ?></p>
        </div>
         
          <div class="col-sm-3 col-xs-6">
          <img src="<?php print theme_get_setting('home_box_2_icon'); ?>" />
          <p><?php print theme_get_setting('home_box_2_message'); ?></p>
       </div>
         
          <div class="col-sm-3 col-xs-6">
          <img src="<?php print theme_get_setting('home_box_3_icon'); ?>" />
          <p><?php print theme_get_setting('home_box_3_message'); ?></p>
      </div>
         
          <div class="col-sm-3 col-xs-6">
         <img src="<?php print theme_get_setting('home_box_4_icon'); ?>" />
          <p><?php print theme_get_setting('home_box_4_message'); ?></p>   
          </div>
       </div>
     </div>
     </div>
     
     <div class="in-section-3">
        <div class="container">
           <div class="row">
              <div class="col-md-12">
                <h3> <?php print theme_get_setting('third_section_h3'); ?>
      <span><?php print theme_get_setting('third_section_subtitle'); ?></span></h3>
      </div>
           
           </div>
           <div class="img-cols section-row">    
             <div class="img-col-1 cols img-1">
               <a href="<?php print theme_get_setting('third_section_box_1_url'); ?>" class="full-bg">
               <span class="full-block full-bg" >
               <span class="text-h-m "> 
                <span class="arrow"></span>
                  <span class="text-h"><?php print theme_get_setting('third_section_box_1_caption'); ?></Flexible Length Coursesspan><span class="text-more"><?php print theme_get_setting('third_section_box_1_roll_text'); ?> →</span></span>
               </span>
               </span>
             </a>
             </div>
             <div class="img-col-1 cols img-2">
               <a href="<?php print theme_get_setting('third_section_box_2_url'); ?>" class="full-bg">
               <span class="full-block full-bg" >
               
               <span class="text-h-m "> 
                <span class="arrow"></span>
                  <span class="text-h"><?php print theme_get_setting('third_section_box_2_caption'); ?></span><span class="text-more"><?php print theme_get_setting('third_section_box_1_roll_text'); ?> →</span></span>
               </span>
             </a>
             </div>
             <div class="img-col-1 cols img-3">
               <a href="<?php print theme_get_setting('third_section_box_3_url'); ?>" class="full-bg">
               <span class="full-block full-bg" >
               <span class="text-h-m "> 
                <span class="arrow"></span>
                  <span class="text-h"><?php print theme_get_setting('third_section_box_3_caption'); ?></span><span class="text-more"><?php print theme_get_setting('third_section_box_1_roll_text'); ?> →</span></span>
               </span>
             </a>
             </div>
           
           <div class="img-col-1 cols img-4">
               <a href="<?php print theme_get_setting('third_section_box_4_url'); ?>" class="full-bg">
               <span class="full-block full-bg" >
               <span class="text-h-m "> 
                <span class="arrow"></span>
                  <span class="text-h"><?php print theme_get_setting('third_section_box_4_caption'); ?></span><span class="text-more"><?php print theme_get_setting('third_section_box_1_roll_text'); ?> →</span></span>
               </span>
             </a>
             </div>
             
             <div class="img-col-1 cols img-5">
               <a href="<?php print theme_get_setting('third_section_box_5_url'); ?>" class="full-bg">
               <span class="full-block full-bg" >
               <span class="text-h-m "> 
                <span class="arrow"></span>
                  <span class="text-h"><?php print theme_get_setting('third_section_box_5_caption'); ?></span><span class="text-more"><?php print theme_get_setting('third_section_box_1_roll_text'); ?> →</span></span>
               </span>
             </a>
             </div>
           </div>  
           <div class="col-md-12">
            <div class="text-center">
             <a class="blue-btn" href="<?php print theme_get_setting('third_section_button_url'); ?>"><?php print theme_get_setting('third_section_box_1_roll_text'); ?> →</a>
             </div>
           </div>
        </div>
     </div>
     
     <div class="in-section-4">
        <div class="container">
           <div class="row">
              <div class="col-md-12">
                <h3> <?php print theme_get_setting('fourth_section_h3'); ?></h3>
      </div>
           </div>
           
           <div class="img-cols section-row">    
             <div class="img-col-1 cols img-1">  
               <a href="<?php print theme_get_setting('fourth_section_box_1_url'); ?>" class="full-bg">
               <span class="full-block full-bg" >
               <span class="text-h-m ">       
               <span class="arrow"></span>
                  <span class="text-h"><?php print theme_get_setting('fourth_section_box_1_caption'); ?></Flexible Length Coursesspan><span class="text-more"><?php print theme_get_setting('fourth_section_box_1_roll_text'); ?> →</span></span>
               </span>
               </span>
             </a>
             </div>
             <div class="img-col-1 wd-50 cols img-2">
               <a href="<?php print theme_get_setting('fourth_section_box_2_url'); ?>" class="full-bg">
               <span class="full-block full-bg" >
               <span class="text-h-m "> 
               <span class="arrow"></span>
                  <span class="text-h"><?php print theme_get_setting('fourth_section_box_2_caption'); ?></Flexible Length Coursesspan><span class="text-more"><?php print theme_get_setting('fourth_section_box_2_roll_text'); ?> →</span></span>
               </span>
             </a>
             </div>
             <div class="img-col-1 cols img-3">
               <a href="<?php print theme_get_setting('fourth_section_box_3_url'); ?>" class="full-bg">
               <span class="full-block full-bg" >
               <span class="text-h-m "> 
               <span class="arrow"></span>
                  <span class="text-h"><?php print theme_get_setting('fourth_section_box_3_caption'); ?></Flexible Length Coursesspan><span class="text-more"><?php print theme_get_setting('fourth_section_box_3_roll_text'); ?> →</span></span>
               </span>
             </a>
             </div>
           
           <div class="img-col-1 wd-50 cols img-4">
               <a href="<?php print theme_get_setting('fourth_section_box_4_url'); ?>" class="full-bg">
               <span class="full-block full-bg" >
               <span class="text-h-m "> 
               <span class="arrow"></span>
                  <span class="text-h"><?php print theme_get_setting('fourth_section_box_4_caption'); ?></Flexible Length Coursesspan><span class="text-more"><?php print theme_get_setting('fourth_section_box_4_roll_text'); ?> →</span></span>
               </span>
             </a>
             </div>
             
             <div class="img-col-1 cols img-5">
               <a href="<?php print theme_get_setting('fourth_section_box_5_url'); ?>" class="full-bg">
               <span class="full-block full-bg" >
               <span class="text-h-m "> 
               <span class="arrow"></span>
                  <span class="text-h"><?php print theme_get_setting('fourth_section_box_5_caption'); ?></Flexible Length Coursesspan><span class="text-more"><?php print theme_get_setting('fourth_section_box_5_roll_text'); ?> →</span></span>
               </span>
             </a>
             </div>
             <div class="img-col-1 cols img-6">
               <a href="<?php print theme_get_setting('fourth_section_box_6_url'); ?>" class="full-bg">
               <span class="full-block full-bg" >
               <span class="text-h-m "> 
               <span class="arrow"></span>
                  <span class="text-h"><?php print theme_get_setting('fourth_section_box_6_caption'); ?></Flexible Length Coursesspan><span class="text-more"><?php print theme_get_setting('fourth_section_box_6_roll_text'); ?> →</span></span>
               </span>
             </a>
             </div>
           </div>
           <div class="col-md-12">
            <div class="text-center">
               <a class="blue-btn" href="<?php print theme_get_setting('fourth_section_button_url'); ?>"><?php print theme_get_setting('fourth_section_button_text'); ?> →</a>
            </div>
           </div>
        </div>
     </div>
  
     
    <?php include DRUPAL_ROOT . '/' . path_to_theme() . '/templates/inc/footer.inc'; ?> 
 
   
     
</div>
<script src="js/jquery/jquery.min.js"></script> 
<script src="js/bootstrap.bundle.min.js"></script> 
<script src="js/animate.min.js"></script> 
<script type="text/javascript" src="js/bootstrap-tabcollapse.js"></script> 
<script>

$('.tab-content').on('hidden.bs.collapse', function () {
 alert(1)
})
$(".tab-menu-content ul li a[href^='#']").on('click', function(e) {

   // prevent default anchor click behavior
   e.preventDefault();

   // store hash
   var hash = this.hash;

   // animate
   $('html, body').animate({
       scrollTop: $(hash).offset().top
     }, 1000, function(){

       // when done, add hash to url
       // (default click behaviour)
       window.location.hash = hash;
     });

});

$(".footer .link-row .goto-top a[href^='#']").on('click', function(e) {

   // prevent default anchor click behavior
   e.preventDefault();

   // store hash
   var hash = this.hash;

   // animate
   $('html, body').animate({
       scrollTop: $(hash).offset().top
     }, 1000, function(){

       // when done, add hash to url
       // (default click behaviour)
       window.location.hash = hash;
     });

});
</script> 

<!--<script>
/*global $ */
$(document).ready(function () {

    "use strict";

    $('.menu > ul > li:has( > ul)').addClass('menu-dropdown-icon');
    //Checks if li has sub (ul) and adds class for toggle icon - just an UI


    $('.menu > ul > li > ul:not(:has(ul))').addClass('normal-sub');
    //Checks if drodown menu's li elements have anothere level (ul), if not the dropdown is shown as regular dropdown, not a mega menu (thanks Luka Kladaric)

    $(".menu > ul").before("<a href=\"#\" class=\"menu-mobile\"></a>");

    //Adds menu-mobile class (for mobile toggle menu) before the normal menu
    //Mobile menu is hidden if width is more then 959px, but normal menu is displayed
    //Normal menu is hidden if width is below 959px, and jquery adds mobile menu
    //Done this way so it can be used with wordpress without any trouble

    $(".menu > ul > li").hover(function (e) {
        if ($(window).width() > 943) {
            $(this).children("ul").stop(true, false).fadeToggle(150);
            e.preventDefault();
        }
    });
    //If width is more than 943px dropdowns are displayed on hover

    $(".menu > ul > li").click(function () {
        if ($(window).width() <= 943) {
            $(this).children("ul").fadeToggle(150);
        }
    });
    //If width is less or equal to 943px dropdowns are displayed on click (thanks Aman Jain from stackoverflow)

    $(".menu-mobile").click(function (e) {
        $(".menu > ul").toggleClass('show-on-mobile');
        e.preventDefault();
    });
    //when clicked on mobile-menu, normal menu is shown as a list, classic rwd menu story (thanks mwl from stackoverflow)

});
</script>--> 
<script>
(function($) {
$.fn.menumaker = function(options) {  
 var cssmenu = $(this), settings = $.extend({
   format: "dropdown",
   sticky: false
 }, options);
 return this.each(function() {
   $(this).find(".button").on('click', function(){
     $(this).toggleClass('menu-opened');
     var mainmenu = $(this).next('ul');
     if (mainmenu.hasClass('open')) { 
       mainmenu.slideToggle().removeClass('open');
     }
     else {
       mainmenu.slideToggle().addClass('open');
       if (settings.format === "dropdown") {
         mainmenu.find('ul').show();
       }
     }
   });
   cssmenu.find('li ul').parent().addClass('has-sub');
multiTg = function() {
     cssmenu.find(".has-sub").on('click', function() {
  //    $(this).toggleClass('submenu-opened');
   
       if ($(this).find('ul').hasClass('open')) {
         $(this).find('ul').removeClass('open').slideToggle();
		 $(".has-sub").find('ul.open').removeClass('open').hide();
       }
       else {
		   $(".has-sub").find('ul.open').removeClass('open').hide();
         $(this).find('ul').addClass('open').slideToggle();
       }
	      
	   
     });
   };
   if (settings.format === 'multitoggle') multiTg();
   else cssmenu.addClass('dropdown');
   if (settings.sticky === true) cssmenu.css('position', 'fixed');
resizeFix = function() {
  var mediasize = 1000;
     if ($( window ).width() > mediasize) {
       cssmenu.find('ul').show();
     }
     if ($(window).width() <= mediasize) {
       cssmenu.find('ul').hide().removeClass('open');
     }
   };
   resizeFix();
   return $(window).on('resize', resizeFix);
 });
  };
})(jQuery);

(function($){
$(document).ready(function(){
$("#cssmenu").menumaker({
   format: "multitoggle"
});
});
})(jQuery);

</script> 
<script>
$(document).ready(function() {

// Gets the video src from the data-src on each button

var $videoSrc;  
$('.video-btn').click(function() {
    $videoSrc = $(this).data( "https://www.youtube.com/watch?v=IadsLclBOS8" );
});
console.log($videoSrc);

  
  
// when the modal is opened autoplay it  
$('#myModal').on('shown.bs.modal', function (e) {
    
// set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
$("#video").attr('src',$videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1" ); 
})
  
  
// stop playing the youtube video when I close the modal
$('#myModal').on('hide.bs.modal', function (e) {
    // a poor man's stop video
    $("#video").attr('src',$videoSrc); 
}) 
// document ready  
});
</script> 
<script>
  $('.carousel').carousel();
$('.panel-heading a').click(function() {
    $('.panel-heading').removeClass('active');
    
    //If the panel was open and would be closed by this click, do not active it
    if(!$(this).closest('.sub-catg').find('.collapse ').hasClass('show'))
        $(this).parents('.panel-heading').addClass('active');
 });
</script>