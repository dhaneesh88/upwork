<?php

require_once 'template.theme_webform.inc.php';

/**
 * Add body classes if certain regions have content.
 */
function skytheme_preprocess_html(&$variables) {
//  if (!empty($variables['page']['featured'])) {
//    $variables['classes_array'][] = 'featured';
//  }
//  // Add conditional stylesheets for IE
//  drupal_add_css(path_to_theme() . '/css/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'preprocess' => FALSE));
//  drupal_add_css(path_to_theme() . '/css/ie6.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 6', '!IE' => FALSE), 'preprocess' => FALSE));
}

function skytheme_preprocess_page(&$variables) {
//  $variables['main_nav'] = theme( 'links',
//          array('links' => menu_navigation_links('main-menu'),
//                'attributes' => array('class' => 'menu')));

//  $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
//  $variables['main_menu'] =  $main_menu_tree;
  $variables['add_extra_wrap_class'] = NULL;
  $variables['add_wrap_id'] = NULL;
  if (user_is_logged_in()) {
    $variables['adjst_top_space'] =  'adjst-cnt-top';
  }
  if (isset($variables['node']) && $variables['node']->type == 'school') {
    $variables['add_extra_wrap_class'] = 'english-school';
  }
  if (arg(0) ==  "taxonomy" && arg(1) == "term" && is_numeric(arg(2)) && arg(3) == "") {
  	$variables['add_wrap_id'] = 'top-hed';
  }
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'school_objects')
    ->entityCondition('bundle', 'menu');
  $result = $query->execute();
  $menu_entity = array_keys($result['school_objects']);
  $variables['menu_items_entities'] = entity_metadata_wrapper('school_objects', $menu_entity[0], array('bundle' => 'menu'));
}

/**
 * Implements HOOK_form_webform_client_form_NID_alter().
 */
function skytheme_form_webform_client_form_652_alter(&$form, &$form_state, $form_id) {

  //$form['#prefix'] = '<div class="form-bg"><div class="head">1 - Courses you are interested in</div><div class="container"><div class="row">';
  //$form['#suffix'] = '</div></div></div>';

  $form['#attributes']['css'] = '';

  $form['submitted']['what_would_you_like_to_study']['#prefix'] = '<div class="form-bg"><div class="head">1 - Courses you are interested in</div><div class="container"><div class="row"><div class="col-lg-6 col-md-12">';
  $form['submitted']['what_would_you_like_to_study']['#suffix'] = '</div>';
  $form['submitted']['what_would_you_like_to_study']['#wrapper_attributes']['class'] = array('form-group sep');
  $form['submitted']['what_would_you_like_to_study']['#attributes']['class'] = array('form-control');

  $form['submitted']['how_long_would_you_like_to_study_for']['#prefix'] = '<div class="col-lg-6 col-md-12">';
  $form['submitted']['how_long_would_you_like_to_study_for']['#suffix'] = '</div>';
  $form['submitted']['how_long_would_you_like_to_study_for']['#wrapper_attributes']['class'] = array('form-group sep');
  $form['submitted']['how_long_would_you_like_to_study_for']['#attributes']['class'] = array('form-control');

  $form['submitted']['when_would_you_like_to_start']['#prefix'] = '<div class="col-lg-6 col-md-12">';
  $form['submitted']['when_would_you_like_to_start']['#suffix'] = '</div>';
  $form['submitted']['when_would_you_like_to_start']['#wrapper_attributes']['class'] = array('form-group sep');
  $form['submitted']['when_would_you_like_to_start']['#attributes']['class'] = array('form-control');

  $form['submitted']['where_would_you_like_to_study_english']['#prefix'] = '<div class="col-lg-6 col-md-12">';
  $form['submitted']['where_would_you_like_to_study_english']['#suffix'] = '</div>';
  $form['submitted']['where_would_you_like_to_study_english']['#wrapper_attributes']['class'] = array('form-group sep');
  $form['submitted']['where_would_you_like_to_study_english']['#attributes']['class'] = array('form-control');

  $form['submitted']['where_do_you_currently_live']['#prefix'] = '<div class="col-lg-6 col-md-12">';
  $form['submitted']['where_do_you_currently_live']['#suffix'] = '</div>';
  $form['submitted']['where_do_you_currently_live']['#wrapper_attributes']['class'] = array('form-group sep');
  $form['submitted']['where_do_you_currently_live']['#attributes']['class'] = array('form-control');

  $form['submitted']['what_is_your_nationality']['#prefix'] = '<div class="col-lg-6 col-md-12">';
  $form['submitted']['what_is_your_nationality']['#suffix'] = '</div>';
  $form['submitted']['what_is_your_nationality']['#wrapper_attributes']['class'] = array('form-group sep');
  $form['submitted']['what_is_your_nationality']['#attributes']['class'] = array('form-control');

  $form['submitted']['any_questions_for_our_team']['#prefix'] = '<div class="col-lg-12 col-md-12">';
  $form['submitted']['any_questions_for_our_team']['#wrapper_attributes']['class'] = array('form-group');
  $form['submitted']['any_questions_for_our_team']['#attributes']['class'] = array('form-control');
  $form['submitted']['any_questions_for_our_team']['#suffix'] = '</div></div></div></div>';

  // Second section
  $form['submitted']['first_name']['#prefix'] = '<div class="form-bg"><div class="head">2 - Your details</div><div class="container"><div class="row"><div class="col-lg-6 col-md-12">';
  $form['submitted']['first_name']['#suffix'] = '</div>';
  $form['submitted']['first_name']['#wrapper_attributes']['class'] = array('form-group');
  $form['submitted']['first_name']['#attributes']['class'] = array('form-control');

  $form['submitted']['last_name']['#prefix'] = '<div class="col-lg-6 col-md-12">';
  $form['submitted']['last_name']['#suffix'] = '</div>';
  $form['submitted']['last_name']['#wrapper_attributes']['class'] = array('form-group');
  $form['submitted']['last_name']['#attributes']['class'] = array('form-control');
  

  $form['submitted']['email']['#prefix'] = '<div class="col-lg-6 col-md-12">';
  $form['submitted']['email']['#wrapper_attributes']['class'] = array('form-group');
  $form['submitted']['email']['#attributes']['class'] = array('form-control');
  $form['submitted']['email']['#suffix'] = '</div>';

  $form['submitted']['phone']['#prefix'] = '<div class="col-lg-6 col-md-12">';
  $form['submitted']['phone']['#wrapper_attributes']['class'] = array('form-group');
  $form['submitted']['phone']['#attributes']['class'] = array('form-control');
  $form['submitted']['phone']['#suffix'] = '</div>';


  $form['submitted']['preferred_contact_time']['#prefix'] = '<div class="col-md-6">';
  $form['submitted']['preferred_contact_time']['#wrapper_attributes']['class'] = array('form-group');
  $form['submitted']['preferred_contact_time']['#attributes']['class'] = array('form-control');
  $form['submitted']['preferred_contact_time']['#suffix'] = '</div></div></div></div>';

  $form['submitted']['join_our_mailing_list_for_offers']['#prefix'] = '<div class="text-right right-check">';
  $form['submitted']['join_our_mailing_list_for_offers']['#wrapper_attributes']['class'] = array('form-check');
  $form['submitted']['join_our_mailing_list_for_offers']['Yes']['#attributes']['class'] = array('form-check-input');

  $form['submitted']['we_work_with_authorised_resellers_to_offer_you']['#wrapper_attributes']['class'] = array('form-check');
  $form['submitted']['we_work_with_authorised_resellers_to_offer_you']['Yes']['#attributes']['class'] = array('form-check-input');

  $form['submitted']['by_clicking_submit_i_am_confirming_i_am_over_the_age_of_16']['#markup'] = '<p style="text-align: right;">By clicking submit, I am confirming I am over the age of 16.</p>';
  $form['submitted']['by_clicking_submit_i_am_confirming_i_am_over_the_age_of_16']['#wrapper_attributes']['class'] = array('form-check');


  $form['actions']['submit']['#attributes']['class'] = array('webform-submit button-primary btn btn-default form-submit');
  $form['actions']['submit']['#value'] = 'Contact a Student Advisor';
  $form['actions']['#suffix'] = '</div>';

}
/**
* implement form alter
* 
*/
function skytheme_form_alter(&$form, &$form_state, $form_id) {

  if ($form_id == 'simplenews_block_form_489') {

    $form['mail']['#theme_wrappers'] = array();
    $form['mail']['#attributes']['placeholder'] = 'Enter your email';
    $form['mail']['#attributes']['class'] = array('form-control form-text');
    $form['mail']['#size'] = 60;
    $form['mail']['#maxlength'] = 128;

    $form['submit']['#attributes']['class'] = array('btn btn-default form-submit');
    $form['submit']['#value'] = 'Sign Up';

    $form['submit']['#ajax'] = array(
      'callback' => '_dd_simplenews_block_form_ajax_submit',
      'wrapper' => 'block-simplenews-kalpan',
      'method' => 'replace',
      'effect' => 'fade',
      'progress' => array('type' => 'none'),
    );
    $form['submit']['#executes_submit_callback'] = TRUE;
    unset($form['#submit']);
    unset($form['#validate']);

  }
}

/**
 * Ajax callback to reload the image field
 */
function _dd_simplenews_block_form_ajax_submit($form, &$form_state) {
  $return = '<div id="block-simplenews-kalpan" class="block block-simplenews first odd">';
  //$return .= render($form);
  $return .= '</div>';
  if (!valid_email_address($form_state['values']['mail'])) {
    $return .= '<section class="errors-primal">' . t('The e-mail address is not valid.') . '</section>';
    return $return;
  }
  else {
    if (module_exists('simplenews')) {
      module_load_include('inc', 'simplenews', 'views/simplenews.subscription');
      $tid = $form['#tid'];
      $account = simplenews_load_user_by_mail($form_state['values']['mail']);
      $confirm = simplenews_require_double_opt_in($tid, $account);
      $subscription = simplenews_subscribe_user($form_state['values']['mail'], $tid, $confirm, 'website');
      $return .= '<section class="confirm-primal">' . t('You have been subscribed successfully.') . '</section>';
      return $return;
    }
  }
}