<?php

// Homepage Settings.
$form['homepage_settings'] = array(
  '#type' => 'fieldset',
  '#title' => t('Homepage Settings'),
  '#group' => 'bootstrap',
);

// Hero Banners.
$form['homepage_settings']['hero_banners'] = array(
  '#type' => 'fieldset',
  '#title' => t('Hero Banners'),
  '#collapsible' => TRUE,
  '#collapsed' => TRUE,
);


// Top Hero Stuff
$form['homepage_settings']['hero_banners']['hero_logo'] = array(
  '#type' => 'textfield',
  '#title' => 'Hero Logo',
  '#default_value' => theme_get_setting('hero_logo'),
);
$form['homepage_settings']['hero_banners']['hero_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Hero Text',
  '#default_value' => theme_get_setting('hero_text'),
);
$form['homepage_settings']['hero_banners']['hero_subtitle'] = array(
  '#type' => 'textfield',
  '#title' => 'Hero Subtitle',
  '#default_value' => theme_get_setting('hero_subtitle'),
);
$form['homepage_settings']['hero_banners']['hero_button_1_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Hero Button 1 Text',
  '#default_value' => theme_get_setting('hero_button_1_text'),
);
$form['homepage_settings']['hero_banners']['hero_button_1_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Hero Button 1 Url',
  '#default_value' => theme_get_setting('hero_button_1_url'),
);
$form['homepage_settings']['hero_banners']['hero_button_2_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Hero Button 2 Text',
  '#default_value' => theme_get_setting('hero_button_2_text'),
);
$form['homepage_settings']['hero_banners']['hero_button_2_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Hero Button 2 Url',
  '#default_value' => theme_get_setting('hero_button_2_url'),
);
$form['homepage_settings']['hero_banners']['hero_button_2_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Hero Button 2 Url',
  '#default_value' => theme_get_setting('hero_button_2_url'),
);
$form['homepage_settings']['hero_banners']['right_box_1_caption'] = array(
  '#type' => 'textfield',
  '#title' => 'Right Box 1 Caption',
  '#default_value' => theme_get_setting('right_box_1_caption'),
);
$form['homepage_settings']['hero_banners']['right_box_1_roll_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Right Box 1 Rollover Text',
  '#default_value' => theme_get_setting('right_box_1_roll_text'),
);
$form['homepage_settings']['hero_banners']['right_box_1_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Right Box 1 URL',
  '#default_value' => theme_get_setting('right_box_1_url'),
);
$form['homepage_settings']['hero_banners']['right_box_2_caption'] = array(
  '#type' => 'textfield',
  '#title' => 'Right Box 2 Caption',
  '#default_value' => theme_get_setting('right_box_2_caption'),
);
$form['homepage_settings']['hero_banners']['right_box_2_roll_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Right Box 2 Rollover Text',
  '#default_value' => theme_get_setting('right_box_2_roll_text'),
);
$form['homepage_settings']['hero_banners']['right_box_2_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Right Box 2 URL',
  '#default_value' => theme_get_setting('right_box_2_url'),
);
$form['homepage_settings']['hero_banners']['right_box_3_caption'] = array(
  '#type' => 'textfield',
  '#title' => 'Right Box 3 Caption',
  '#default_value' => theme_get_setting('right_box_3_caption'),
);
$form['homepage_settings']['hero_banners']['right_box_3_roll_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Right Box 3 Rollover Text',
  '#default_value' => theme_get_setting('right_box_3_roll_text'),
);
$form['homepage_settings']['hero_banners']['right_box_3_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Right Box 3 URL',
  '#default_value' => theme_get_setting('right_box_3_url'),
);

// Second section with logos and main message


// Second Section.
$form['homepage_settings']['second_section'] = array(
  '#type' => 'fieldset',
  '#title' => t('Second Section'),
  '#collapsible' => TRUE,
  '#collapsed' => TRUE,
);

$form['homepage_settings']['second_section']['homepage_h2'] = array(
  '#type' => 'textfield',
  '#title' => 'Homepage H2 Title',
  '#default_value' => theme_get_setting('homepage_h2'),
);

$form['homepage_settings']['second_section']['homepage_subtitle'] = array(
  '#type' => 'textfield',
  '#title' => 'Homepage Subtitle',
  '#default_value' => theme_get_setting('homepage_subtitle'),
);

$form['homepage_settings']['second_section']['home_box_1_icon'] = array(
  '#type' => 'textfield',
  '#title' => 'Home Box 1 Icon',
  '#default_value' => theme_get_setting('home_box_1_icon'),
);

$form['homepage_settings']['second_section']['home_box_1_message'] = array(
  '#type' => 'textfield',
  '#title' => 'Home Box 1 Message',
  '#default_value' => theme_get_setting('home_box_1_message'),
);

$form['homepage_settings']['second_section']['home_box_2_icon'] = array(
  '#type' => 'textfield',
  '#title' => 'Home Box 2 Icon',
  '#default_value' => theme_get_setting('home_box_2_icon'),
);

$form['homepage_settings']['second_section']['home_box_2_message'] = array(
  '#type' => 'textfield',
  '#title' => 'Home Box 2 Message',
  '#default_value' => theme_get_setting('home_box_2_message'),
);

$form['homepage_settings']['second_section']['home_box_3_icon'] = array(
  '#type' => 'textfield',
  '#title' => 'Home Box 3 Icon',
  '#default_value' => theme_get_setting('home_box_3_icon'),
);

$form['homepage_settings']['second_section']['home_box_3_message'] = array(
  '#type' => 'textfield',
  '#title' => 'Home Box 3 Message',
  '#default_value' => theme_get_setting('home_box_3_message'),
);

$form['homepage_settings']['second_section']['home_box_4_icon'] = array(
  '#type' => 'textfield',
  '#title' => 'Home Box 4 Icon',
  '#default_value' => theme_get_setting('home_box_4_icon'),
);

$form['homepage_settings']['second_section']['home_box_4_message'] = array(
  '#type' => 'textfield',
  '#title' => 'Home Box 4 Message',
  '#default_value' => theme_get_setting('home_box_4_message'),
);




// Third section.

$form['homepage_settings']['third_section'] = array(
  '#type' => 'fieldset',
  '#title' => t('Third Section'),
  '#collapsible' => TRUE,
  '#collapsed' => TRUE,
);

$form['homepage_settings']['third_section']['third_section_h3'] = array(
  '#type' => 'textfield',
  '#title' => 'Third Section H3',
  '#default_value' => theme_get_setting('third_section_h3'),
);

$form['homepage_settings']['third_section']['third_section_subtitle'] = array(
  '#type' => 'textfield',
  '#title' => 'Third Section Subtitle',
  '#default_value' => theme_get_setting('third_section_subtitle'),
);

$form['homepage_settings']['third_section']['third_section_box_1_caption'] = array(
  '#type' => 'textfield',
  '#title' => 'Third Section Box 1 Caption',
  '#default_value' => theme_get_setting('third_section_box_1_caption'),
);

$form['homepage_settings']['third_section']['third_section_box_1_roll_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Third Section Box 1 Roll Text',
  '#default_value' => theme_get_setting('third_section_box_1_roll_text'),
);
$form['homepage_settings']['third_section']['third_section_box_1_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Third Section Box 1 URL',
  '#default_value' => theme_get_setting('third_section_box_1_url'),
);
$form['homepage_settings']['third_section']['third_section_box_2_caption'] = array(
  '#type' => 'textfield',
  '#title' => 'Third Section Box 2 Caption',
  '#default_value' => theme_get_setting('third_section_box_2_caption'),
);

$form['homepage_settings']['third_section']['third_section_box_2_roll_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Third Section Box 2 Roll Text',
  '#default_value' => theme_get_setting('third_section_box_2_roll_text'),
);
$form['homepage_settings']['third_section']['third_section_box_2_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Third Section Box 2 URL',
  '#default_value' => theme_get_setting('third_section_box_2_url'),
);

$form['homepage_settings']['third_section']['third_section_box_3_caption'] = array(
  '#type' => 'textfield',
  '#title' => 'Third Section Box 3 Caption',
  '#default_value' => theme_get_setting('third_section_box_3_caption'),
);

$form['homepage_settings']['third_section']['third_section_box_3_roll_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Third Section Box 3 Roll Text',
  '#default_value' => theme_get_setting('third_section_box_3_roll_text'),
);
$form['homepage_settings']['third_section']['third_section_box_3_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Third Section Box 3 URL',
  '#default_value' => theme_get_setting('third_section_box_3_url'),
);

$form['homepage_settings']['third_section']['third_section_box_4_caption'] = array(
  '#type' => 'textfield',
  '#title' => 'Third Section Box 4 Caption',
  '#default_value' => theme_get_setting('third_section_box_4_caption'),
);

$form['homepage_settings']['third_section']['third_section_box_4_roll_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Third Section Box 4 Roll Text',
  '#default_value' => theme_get_setting('third_section_box_4_roll_text'),
);
$form['homepage_settings']['third_section']['third_section_box_4_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Third Section Box 4 URL',
  '#default_value' => theme_get_setting('third_section_box_4_url'),
);

$form['homepage_settings']['third_section']['third_section_box_5_caption'] = array(
  '#type' => 'textfield',
  '#title' => 'Third Section Box 5 Caption',
  '#default_value' => theme_get_setting('third_section_box_5_caption'),
);

$form['homepage_settings']['third_section']['third_section_box_5_roll_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Third Section Box 5 Roll Text',
  '#default_value' => theme_get_setting('third_section_box_5_roll_text'),
);
$form['homepage_settings']['third_section']['third_section_box_5_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Third Section Box 5 URL',
  '#default_value' => theme_get_setting('third_section_box_5_url'),
);
$form['homepage_settings']['third_section']['third_section_button_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Third Section Button Text',
  '#default_value' => theme_get_setting('third_section_button_text'),
);
$form['homepage_settings']['third_section']['third_section_button_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Third Section Button URL',
  '#default_value' => theme_get_setting('third_section_button_url'),
);

// Fourth section.

$form['homepage_settings']['fourth_section'] = array(
  '#type' => 'fieldset',
  '#title' => t('Fourth Section'),
  '#collapsible' => TRUE,
  '#collapsed' => TRUE,
);
$form['homepage_settings']['fourth_section']['fourth_section_h3'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section H3',
  '#default_value' => theme_get_setting('fourth_section_h3'),
);
$form['homepage_settings']['fourth_section']['fourth_section_box_1_caption'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section Box 1 Caption',
  '#default_value' => theme_get_setting('fourth_section_box_1_caption'),
);

$form['homepage_settings']['fourth_section']['fourth_section_box_1_roll_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section Box 1 Roll Text',
  '#default_value' => theme_get_setting('fourth_section_box_1_roll_text'),
);
$form['homepage_settings']['fourth_section']['fourth_section_box_1_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section Box 1 URL',
  '#default_value' => theme_get_setting('fourth_section_box_1_url'),
);

$form['homepage_settings']['fourth_section']['fourth_section_box_2_caption'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section Box 2 Caption',
  '#default_value' => theme_get_setting('fourth_section_box_2_caption'),
);

$form['homepage_settings']['fourth_section']['fourth_section_box_2_roll_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section Box 2 Roll Text',
  '#default_value' => theme_get_setting('fourth_section_box_2_roll_text'),
);
$form['homepage_settings']['fourth_section']['fourth_section_box_2_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section Box 2 URL',
  '#default_value' => theme_get_setting('fourth_section_box_2_url'),
);

$form['homepage_settings']['fourth_section']['fourth_section_box_3_caption'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section Box 3 Caption',
  '#default_value' => theme_get_setting('fourth_section_box_3_caption'),
);

$form['homepage_settings']['fourth_section']['fourth_section_box_3_roll_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section Box 3 Roll Text',
  '#default_value' => theme_get_setting('fourth_section_box_3_roll_text'),
);
$form['homepage_settings']['fourth_section']['fourth_section_box_3_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section Box 3 URL',
  '#default_value' => theme_get_setting('fourth_section_box_3_url'),
);

$form['homepage_settings']['fourth_section']['fourth_section_box_4_caption'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section Box 4 Caption',
  '#default_value' => theme_get_setting('fourth_section_box_4_caption'),
);

$form['homepage_settings']['fourth_section']['fourth_section_box_4_roll_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section Box 4 Roll Text',
  '#default_value' => theme_get_setting('fourth_section_box_4_roll_text'),
);
$form['homepage_settings']['fourth_section']['fourth_section_box_4_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section Box 4 URL',
  '#default_value' => theme_get_setting('fourth_section_box_4_url'),
);

$form['homepage_settings']['fourth_section']['fourth_section_box_5_caption'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section Box 5 Caption',
  '#default_value' => theme_get_setting('fourth_section_box_5_caption'),
);

$form['homepage_settings']['fourth_section']['fourth_section_box_5_roll_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section Box 5 Roll Text',
  '#default_value' => theme_get_setting('fourth_section_box_5_roll_text'),
);
$form['homepage_settings']['fourth_section']['fourth_section_box_5_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section Box 5 URL',
  '#default_value' => theme_get_setting('fourth_section_box_5_url'),
);
$form['homepage_settings']['fourth_section']['fourth_section_box_6_caption'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section Box 6 Caption',
  '#default_value' => theme_get_setting('fourth_section_box_6_caption'),
);

$form['homepage_settings']['fourth_section']['fourth_section_box_6_roll_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section Box 6 Roll Text',
  '#default_value' => theme_get_setting('fourth_section_box_6_roll_text'),
);
$form['homepage_settings']['fourth_section']['fourth_section_box_6_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section Box 1 URL',
  '#default_value' => theme_get_setting('fourth_section_box_6_url'),
);

// Fourth section bottom button.


$form['homepage_settings']['fourth_section']['fourth_section_button_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section Button Text',
  '#default_value' => theme_get_setting('fourth_section_button_text'),
);
$form['homepage_settings']['fourth_section']['fourth_section_button_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Fourth Section Button URL',
  '#default_value' => theme_get_setting('fourth_section_button_url'),
);



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Footer.
$form['footer_settings'] = array(
  '#type' => 'fieldset',
  '#title' => t('Footer Settings'),
  '#group' => 'bootstrap',
);

// First Row.
$form['footer_settings']['footer_first_row'] = array(
  '#type' => 'fieldset',
  '#title' => t('Footer First Row'),
  '#collapsible' => TRUE,
  '#collapsed' => TRUE,
);
// This is the newsletter sign up.
$form['footer_settings']['footer_first_row']['footer_headline'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer First Row Headline',
  '#default_value' => theme_get_setting('footer_headline'),
);
$form['footer_settings']['footer_first_row']['footer_subtitle'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer First Row Subtitle',
  '#default_value' => theme_get_setting('footer_subtitle'),
);
$form['footer_settings']['footer_first_row']['footer_sign_up_cta'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer First Row Sign up CTA button',
  '#default_value' => theme_get_setting('footer_sign_up_cta'),
);


// Second Row. First Set Of 5 links.
$form['footer_settings']['footer_second_row_first_links'] = array(
  '#type' => 'fieldset',
  '#title' => t('Footer Second Row First Links'),
  '#collapsible' => TRUE,
  '#collapsed' => TRUE,
);
$form['footer_settings']['footer_second_row_first_links']['footer_link_1_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer First Link Text',
  '#default_value' => theme_get_setting('footer_link_1_text'),
);
$form['footer_settings']['footer_second_row_first_links']['footer_link_1_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer First Link URL',
  '#default_value' => theme_get_setting('footer_link_1_url'),
);
$form['footer_settings']['footer_second_row_first_links']['footer_link_2_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Second Link Text',
  '#default_value' => theme_get_setting('footer_link_2_text'),
);
$form['footer_settings']['footer_second_row_first_links']['footer_link_2_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Second Link URL',
  '#default_value' => theme_get_setting('footer_link_2_url'),
);
$form['footer_settings']['footer_second_row_first_links']['footer_link_3_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Third Link Text',
  '#default_value' => theme_get_setting('footer_link_3_text'),
);
$form['footer_settings']['footer_second_row_first_links']['footer_link_3_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Third Link URL',
  '#default_value' => theme_get_setting('footer_link_3_url'),
);
$form['footer_settings']['footer_second_row_first_links']['footer_link_4_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Fourth Link Text',
  '#default_value' => theme_get_setting('footer_link_4_text'),
);
$form['footer_settings']['footer_second_row_first_links']['footer_link_4_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Fourth Link URL',
  '#default_value' => theme_get_setting('footer_link_4_url'),
);
$form['footer_settings']['footer_second_row_first_links']['footer_link_5_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Fifth Link Text',
  '#default_value' => theme_get_setting('footer_link_5_text'),
);
$form['footer_settings']['footer_second_row_first_links']['footer_link_5_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Fifth Link URL',
  '#default_value' => theme_get_setting('footer_link_5_url'),
);



// Second Row. Second Set Of 7 links.
$form['footer_settings']['footer_second_row_second_links'] = array(
  '#type' => 'fieldset',
  '#title' => t('Footer Second Row Second Links'),
  '#collapsible' => TRUE,
  '#collapsed' => TRUE,
);
$form['footer_settings']['footer_second_row_second_links']['footer_link_2R_1_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer First Link Text',
  '#default_value' => theme_get_setting('footer_link_2R_1_text'),
);
$form['footer_settings']['footer_second_row_second_links']['footer_link_2R_1_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer First Link URL',
  '#default_value' => theme_get_setting('footer_link_2R_1_url'),
);
$form['footer_settings']['footer_second_row_second_links']['footer_link_2R_2_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Second Link Text',
  '#default_value' => theme_get_setting('footer_link_2R_2_text'),
);
$form['footer_settings']['footer_second_row_second_links']['footer_link_2R_2_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Second Link URL',
  '#default_value' => theme_get_setting('footer_link_2R_2_url'),
);
$form['footer_settings']['footer_second_row_second_links']['footer_link_2R_3_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Third Link Text',
  '#default_value' => theme_get_setting('footer_link_2R_3_text'),
);
$form['footer_settings']['footer_second_row_second_links']['footer_link_2R_3_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Third Link URL',
  '#default_value' => theme_get_setting('footer_link_2R_3_url'),
);
$form['footer_settings']['footer_second_row_second_links']['footer_link_2R_4_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Fourth Link Text',
  '#default_value' => theme_get_setting('footer_link_2R_4_text'),
);
$form['footer_settings']['footer_second_row_second_links']['footer_link_2R_4_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Fourth Link URL',
  '#default_value' => theme_get_setting('footer_link_2R_4_url'),
);
$form['footer_settings']['footer_second_row_second_links']['footer_link_2R_5_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Fifth Link Text',
  '#default_value' => theme_get_setting('footer_link_2R_5_text'),
);
$form['footer_settings']['footer_second_row_second_links']['footer_link_2R_5_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Fifth Link URL',
  '#default_value' => theme_get_setting('footer_link_2R_5_url'),
);
$form['footer_settings']['footer_second_row_second_links']['footer_link_2R_6_text'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Sixth Link Text',
  '#default_value' => theme_get_setting('footer_link_2R_6_text'),
);
$form['footer_settings']['footer_second_row_second_links']['footer_link_2R_6_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Sixth Link URL',
  '#default_value' => theme_get_setting('footer_link_2R_6_url'),
);
// Third Row. 2 buttons and phone number
$form['footer_settings']['footer_third_row'] = array(
  '#type' => 'fieldset',
  '#title' => t('Two buttons and phone number'),
  '#collapsible' => TRUE,
  '#collapsed' => TRUE,
);
$form['footer_settings']['footer_third_row']['footer_button_1'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Button 1',
  '#default_value' => theme_get_setting('footer_button_1'),
);
$form['footer_settings']['footer_third_row']['footer_button_1_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Button 1 URL',
  '#default_value' => theme_get_setting('footer_button_1_url'),
);
$form['footer_settings']['footer_third_row']['footer_button_2'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Button 2',
  '#default_value' => theme_get_setting('footer_button_2'),
);
$form['footer_settings']['footer_third_row']['footer_button_2_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Button 2 URL',
  '#default_value' => theme_get_setting('footer_button_2_url'),
);
$form['footer_settings']['footer_third_row']['footer_phone_number'] = array(
  '#type' => 'textfield',
  '#title' => 'Footer Phone Number',
  '#default_value' => theme_get_setting('footer_phone_number'),
);
// Accreditations
$form['footer_settings']['accreditations'] = array(
  '#type' => 'fieldset',
  '#title' => t('Accreditations'),
  '#collapsible' => TRUE,
  '#collapsed' => TRUE,
);
$form['footer_settings']['accreditations']['acc_1'] = array(
  '#type' => 'textfield',
  '#title' => 'Accreditation 1',
  '#default_value' => theme_get_setting('acc_1'),
);
$form['footer_settings']['accreditations']['acc_1_image'] = array(
  '#type' => 'textfield',
  '#title' => 'Accreditation 1 Image',
  '#default_value' => theme_get_setting('acc_1_image'),
);
$form['footer_settings']['accreditations']['acc_1_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Accreditation 1 URL',
  '#default_value' => theme_get_setting('acc_1_url'),
);

$form['footer_settings']['accreditations']['acc_2'] = array(
  '#type' => 'textfield',
  '#title' => 'Accreditation 2',
  '#default_value' => theme_get_setting('acc_2'),
);
$form['footer_settings']['accreditations']['acc_2_image'] = array(
  '#type' => 'textfield',
  '#title' => 'Accreditation 2 Image',
  '#default_value' => theme_get_setting('acc_2_image'),
);
$form['footer_settings']['accreditations']['acc_2_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Accreditation 2 URL',
  '#default_value' => theme_get_setting('acc_2_url'),
);

$form['footer_settings']['accreditations']['acc_3'] = array(
  '#type' => 'textfield',
  '#title' => 'Accreditation 3',
  '#default_value' => theme_get_setting('acc_3'),
);
$form['footer_settings']['accreditations']['acc_3_image'] = array(
  '#type' => 'textfield',
  '#title' => 'Accreditation 3 Image',
  '#default_value' => theme_get_setting('acc_3_image'),
);
$form['footer_settings']['accreditations']['acc_3_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Accreditation 3 URL',
  '#default_value' => theme_get_setting('acc_3_url'),
);

$form['footer_settings']['accreditations']['acc_4'] = array(
  '#type' => 'textfield',
  '#title' => 'Accreditation 4',
  '#default_value' => theme_get_setting('acc_4'),
);
$form['footer_settings']['accreditations']['acc_4_image'] = array(
  '#type' => 'textfield',
  '#title' => 'Accreditation 4 Image',
  '#default_value' => theme_get_setting('acc_4_image'),
);
$form['footer_settings']['accreditations']['acc_4_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Accreditation 4 URL',
  '#default_value' => theme_get_setting('acc_4_url'),
);

$form['footer_settings']['accreditations']['acc_5'] = array(
  '#type' => 'textfield',
  '#title' => 'Accreditation 5',
  '#default_value' => theme_get_setting('acc_5'),
);
$form['footer_settings']['accreditations']['acc_5_image'] = array(
  '#type' => 'textfield',
  '#title' => 'Accreditation 5 Image',
  '#default_value' => theme_get_setting('acc_5_image'),
);
$form['footer_settings']['accreditations']['acc_5_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Accreditation 5 URL',
  '#default_value' => theme_get_setting('acc_5_url'),
);

$form['footer_settings']['accreditations']['acc_6'] = array(
  '#type' => 'textfield',
  '#title' => 'Accreditation 6',
  '#default_value' => theme_get_setting('acc_6'),
);
$form['footer_settings']['accreditations']['acc_6_image'] = array(
  '#type' => 'textfield',
  '#title' => 'Accreditation 6 Image',
  '#default_value' => theme_get_setting('acc_6_image'),
);
$form['footer_settings']['accreditations']['acc_6_url'] = array(
  '#type' => 'textfield',
  '#title' => 'Accreditation 6 URL',
  '#default_value' => theme_get_setting('acc_6_url'),
);