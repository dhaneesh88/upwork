<?php
/**
 * School content type
 */

// HOME>>>SCHOOLS AND DESTINATION>>COUNTRY>>CITY>>SHORT SCHOOL NAME
$language = $node->language;
$node_wrapper = entity_metadata_wrapper('node', $node);
$school_name = trim(str_replace('English school in', '', $node_wrapper->field_h1_title->value()));
$school_coordinates = $node_wrapper->field_coordinates->value();
$hero_image = $node_wrapper->field_hero_image->value();
$main_body = $node_wrapper->field_main_body->value();
$featured_contents = $node_wrapper->field_features_ticked->value();
$term_entity = $node_wrapper->field_course_type->value();
$facilities = $node_wrapper->field_facilities->value();
sort($facilities);
$social_activity = $node_wrapper->field_social_activities->value();
$accreditation = $node_wrapper->field_accreditations->value();
$accreditation_img = $node_wrapper->field_accredimg->value();
$facilities_icon = array('classrooms' => 'icon-ki-faci-classroom',
    'interactive_white_boards' =>'icon-ki-faci-whiteboard',
    'outdoor_area' => 'icon-ki-faci-study',
    'student_lounge' => 'icon-ki-faci-lounge',
    'study_center' => 'icon-ki-faci-garden',
    'wifi' => 'icon-ki-faci-wifi',
    'library' => 'icon-ki-faci-library',
    'computer_lab' => 'icon-ki-faci-computer',
    'sports_facilities' => 'icon-ki-faci-sports',
    'cafeteria' => 'icon-ki-faci-cafe',
    'transport' => 'icon-ki-faci-transport',
    'gym_facilities' => 'icon-ki-faci-gym');

$destination_path = $node_wrapper->field_country->value();
$query = new EntityFieldQuery();
$query->entityCondition('entity_type', 'school_objects')
  ->entityCondition('bundle', 'testimonials')
  ->fieldCondition('field_ref_school', 'target_id', $node->nid)
  ->range(0,1);
$result = $query->execute();
if(isset($result['school_objects'])) {
  $testimonail_entity_id = array_shift(array_keys($result['school_objects']));
  $entity_wrapper = entity_metadata_wrapper('school_objects', $testimonail_entity_id, array('bundle' => 'testimonials'));
  $testimonail_image = $entity_wrapper->field_student_image->value();  
  $testimonail_body = $entity_wrapper->field_testimonial_body->value();
}


?>
<div class="banner-top banner-full" style="background-image:url(<?php echo '/sites/default/files/'.$hero_image['filename']; ?>);">
    <div class="banner-shadow"></div>
    <div class="breadcrumb-wrapper">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item"><a href="/destinations">Schools and Destinations</a></li>
                <li class="breadcrumb-iteme"><a href="<?php print url('taxonomy/term/'.$destination_path[0]->tid); ?>"><?php print $destination_path[0]->name; ?></a></li>
                <li class="breadcrumb-iteme"><a href="<?php print url('taxonomy/term/'.$destination_path[1]->tid); ?>"><?php print $destination_path[1]->name; ?></a></li>
                <li class="breadcrumb-iteme active">
                    <span><?php print $node_wrapper->field_short_school_name->value(); ?></span>
                </li>
            </ol>
        </nav>
    </div>
    <div class="container-fluid">
        <div class="head-subtitle"> <?php echo preg_replace('/\W\w+\s*(\W*)$/', '$1', $node_wrapper->field_h1_title->value()); ?> </div>
        <h2><?php echo strrchr($node_wrapper->field_h1_title->value(),' '); ?></h2>
    </div>
    <div class="banner-content">
        <div class="container-fluid">
            <h6><?php echo $node_wrapper->field_subtitle->value(); ?></h6>
        </div>
    </div>
</div>
<a id="section-region"></a>
<div class="tab-menu-content">
    <div class="container">
        <ul class="smooth-scroll list-unstyled">
            <li><a class="scroll" href="#section-region">Overview</a></li>
            <li><a class="scroll" href="#av-course">Available courses</a></li>
            <li><a class="scroll" href="#school-info">School information</a></li>
            <li><a class="scroll" href="#social-act">Activities</a></li>
            <li><a class="scroll" href="#std-acc">Accommodation</a></li>
            <li><a class="scroll" href="#contact">Contact</a></li>

        </ul>
    </div>
</div>
<div>
    <div class="bath-container">
        <div class="container">
            <div class="bath-head text-center">
                <h2><?php print $node_wrapper->field_h2_title->value(); ?></h2>
                <p><?php print $main_body['value']; ?></p>
            </div>
            <div class="row bathsh-row row-ord">
                <div class="col-lg-6 col-md-6 col-sm-12 ord-1">
                    <div class="left-section">
                        <ul>
                          <?php

                            foreach($featured_contents as $featured_content) {
                               echo '<li>'.$featured_content.'</li>';
                            }
                          ?>
                        </ul>
                        <div class="btn-container">
                            <a href="#" title="FOR MORE INFORMATION" class="btn btn-x1 btn-default">
                                CONTACT AN ADVISOR<span>FOR MORE INFORMATION</span>
                            </a>

                            <a href="#" class="btn btn-x1 btn-text">DOWNLOAD FREE KAPLAN BROCHURE</a>
                        </div>

                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12  ord-2">

                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                    <!-- 16:9 aspect ratio -->
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <video width="400" controls>
                                            <source src="https://youtu.be/668nUCeBHyY"  type="video/mp4">
                                            <!-- <source src="https://www.youtube.com/embed/6uhRxK_EOm4?start=180" type="video/ogg">-->
                                            Your browser does not support HTML5 video. </video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="right-img"  data-toggle="modal" data-target="#myModal"> <img src="/sites/all/themes/skytheme/images/bffbe29690b7a6e1f8f6bc05e701d5f6e9047a52.jpg" />
                        <div class="icon" ><img src="/sites/all/themes/skytheme/images/arrow.png" width="127" height="81" /> <span>2:47</span></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="location-btn"  data-target=""><a href="#schools-m" data-toggle="collapse" aria-expanded="false">Location Information</a></div>
<div class="school-location row-eq-height" id="schools-m" style="height:1;" >
    <div class="left-section">
        <div class="location-text">
            <h3>School location</h3>
            <p><?php print $node_wrapper->field_location_description->value(); ?></p>
        </div>
    </div>
    <div class="right-section">
        <?php if(isset($school_coordinates[0]) && isset($school_coordinates[1])) { ?>
        <iframe width="100%" height="600" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;coord=<?php $school_coordinates[1];?>,<?php $school_coordinates[1];?>&amp;q=<?php print rawurlencode($node_wrapper->field_location_description->value()); ?>+(<?php print rawurlencode($node_wrapper->field_location_description->value()); ?>)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
        <?php } else { ?>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d4980.087792602318!2d-2.3664720571877456!3d51.38387127125508!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf08dc920dcb85a39!2sKaplan+International+English+-+Bath!5e0!3m2!1sen!2sin!4v1540025126115" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        <?php } ?>
    </div>
</div>
<a id="av-course"></a>
<div class="scroll-content">
<ul id="accordion" class="accordion">
    <li>

        <div class="link">Courses & Prices </div>
        <div class="submenu english-course ">
            <h3>English courses at our <?php echo $school_name; ?> school</h3>
            <div class="container">
                <div class="row-container">

<?php foreach($node_wrapper->field_course_type->value() as $courses) { ?>
                        <div class="col-content">
                            <div class="in">
                                <div class="top-row">
                                    <a href="#">
                                        <div class="icon-col">
                                            <span class="icon icon-course-type-flexible-english-courses"></span>
                                        </div>
                                        <div class="head"><?php print $courses->name; ?></div>
                                        <div class="head-tag"><?php print $courses->field_length['und'][0]['value']; ?></div>
                                        <div class="des"><?php print $courses->field_short_description['und'][0]['value']; ?></div>
                                    </a>
                                    <div class="btn-cols">
                                        <div class="btn-1">
                                            <a href="<?php print url('taxonomy/term/'.$courses->tid); ?>">General English course</a>
                                        </div>
                                        <div class="all-btns">
                                            <div class="btn-1"><a href="<?php print url('taxonomy/term/'.$courses->tid); ?>">General English course</a></div>
                                            <div class="btn-1"><a href="<?php print url('taxonomy/term/'.$courses->tid); ?>">General English course</a></div>
                                            <div class="btn-1"><a href="<?php print url('taxonomy/term/'.$courses->tid); ?>">General English course</a></div>
                                        </div>
                                        <a href="<?php print url('taxonomy/term/'.$courses->tid); ?>" class="btn-more">More price examples</a>
                                    </div>
                                </div>
                                <div class="btn-bottom">
                                    <a href="<?php print url('taxonomy/term/'.$courses->tid); ?>" class="btn">Find out more</a>
                                </div>
                            </div>
                       </div>
                    <?php } ?>
            
                </div>
                <div class="btn-container">
                    <a href="#" title="FOR MORE INFORMATION" class="btn btn-x1 btn-default">
                        CONTACT AN ADVISOR<span>FOR MORE INFORMATION</span>
                    </a>
                    <a href="#" class="btn btn-x1 btn-text">DOWNLOAD FREE KAPLAN BROCHURE</a>
                </div>
            </div>
            <?php if(isset($testimonail_body)) { ?>
            <div class="student-testimonial">
                <div class="container">
                    <div class="in-side">
                        <div class="row-container">
                            <div class="left">
                                <img src="<?php print file_create_url($testimonail_image['uri']); ?>"/></div>
                            <div class="right">
                                <?php print $entity_wrapper->field_testimonial_body->value(); ?>
                                <div class="name-detail">
                                  <?php print $entity_wrapper->field_student_name->value(); ?><span>&nbsp;<?php print $entity_wrapper->field_study_details->value(); ?></span>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </li>

    <li>
        <div class="link">About School<i class="fa fa-chevron-down"></i></div>

        <div class="submenu about-bath ">
            <a id="school-info"></a>
            <div class="container">
                <h2> About our <?php echo $school_name; ?> school </h2>

                <div class="row">
                    <div class="col-md-12">
                        <div class="blocks">
                            <div class="icon"><span class="icon icon-school-recommendation"></span></div>
                            <div class="text">Recommendation rate: </div>
                            <div class="text-b"><?php print $node_wrapper->field_recommendation_rate->value(); ?></div>
                        </div><div class="blocks">
                            <div class="icon"><span class="icon icon-school-total-students"></span></div>
                            <div class="text">Average total students: </div>
                            <div class="text-b"><?php print $node_wrapper->field_average_total_students->value(); ?></div>
                        </div><div class="blocks">
                            <div class="icon"><span class="icon icon-school-nationalities"></span></div>
                            <div class="text">Number of nationalities:  </div>
                            <div class="text-b"><?php print $node_wrapper->field_nationality_mix->value(); ?></div>
                        </div><div class="blocks">
                            <div class="icon"><span class="icon icon-school-student-average-age"></span></div>
                            <div class="text">Average age:  </div>
                            <div class="text-b"><?php print $node_wrapper->field_average_age->value(); ?></div>
                        </div><div class="blocks">
                            <div class="icon"><span class="icon icon-school-classrooms"></span></div>
                            <div class="text">Number of classrooms: </div>
                            <div class="text-b"><?php print $node_wrapper->field_number_of_classrooms->value(); ?></div>
                        </div>

                    </div>
                </div>

                <div class="school-facilities">

                    <h3>School facilities</h3>
                    <div class="row">
                        <?php $i = 0; foreach($facilities as $facility) {
                                if($i < 6 ) {
                        ?>
                            <div class="col-md-4 col-sm-12">
                                <div class="content">
                                    <div class="icon"><span class="icon <?php print $facilities_icon[str_replace(' ', '_', strtolower($facility))]; ?>"></span></div><div class="text"><?php echo $facility; ?></div>
                                </div>
                            </div>
                        <?php    }
                            $i++;
                        }
                        ?>
                    </div>
                    <?php if(count($facilities) > 6) { ?>
                    <div class="row">
                        <div id="text">
                      <?php for($i = 6; $i < count($facilities); $i++) {
                        if(isset($facilities[$i])) {
                          ?>
                            <div class="col-md-4 col-sm-12" style="display:inline-block;">
                                <div class="content">
                                    <div class="icon"><span class="icon <?php print $facilities_icon[str_replace(' ', '_', strtolower($facilities[$i]))]; ?>"></span></div><div class="text"><?php echo $facilities[$i]; ?></div>
                                </div>
                            </div>
                        <?php }
                      }
                      ?>
                        </div>
                    </div>
                    <?php } ?>
                  <?php if(count($facilities) > 6) { ?>
                    <div class="btn-section">
                        <button id="toggle">View more facilities +</button>
                    </div>
                  <?php } ?>
                </div>

                <div class="other-info">
                    <h3>Other information</h3>

                    <div class="row">
                        <div class="col-md-8 col-sm-12">
                            <div class="left-sec">
                                <ul>
                                    <?php foreach ($node_wrapper->field_other_information->value() as $other_information) {
                                    print_r($other_information['value']);
                                  } ?>
                                </ul>
                                <ul><li><strong>Find out more about this school </strong><br>
                                        <a class="fact-btn" target="_blank" href="#">Download Fact Sheet<span class="icon icon-ki-download"></span></a></li></ul>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="right-sec">
                                <h2><?php print_r($accreditation[0]); ?></h2>
                                <div class="img-logo">
                                    <div class="col-size">
                                        <div class="cols"><img src="/sites/default/files/<?php print $accreditation_img['filename']; ?>" /></div>
                                    </div>
                                    <div class="col-size">
                                        <div class="cols"> <img src="/sites/default/files/acc-uk-english-uk-new.png" /></div></div>
                                </div>
                                <p><?php print_r($accreditation[1]); if(isset($accreditation[2])) { print $accreditation[2]; }?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="school-gallery">
                <div class="container">
                    <h3>School photo gallery</h3>

                    <p>Check out images of student life at <?php echo $school_name; ?> school</p>
                </div>
                <div class="photo-row">
                    <?php $i = 0; foreach($node_wrapper->field_gallery_images->value() as $gallery_images) {
                        if($i < 4) {
                        ?>
                        <div class="cols">
                            <div class="photos"  data-toggle="modal" data-target="#gallery-p">
                                <div class="" >
                                    <img src="<?php echo '/sites/default/files/'.str_replace('public://', '', $gallery_images['uri']); ?>"/></div>
                            </div>
                        </div>
                    <?php } $i++; } ?>
                </div>
                <div class="grey-bg">
                    <div class="btn-section">
                        <a class="btn-view" href="#"  data-toggle="modal" data-target="#gallery-p">
                            <span>View all photos</span>
                        </a>
                    </div>
                    <div class="btn-container">
                        <a href="#" title="FOR MORE INFORMATION" class="btn btn-x1 btn-default">
                            CONTACT AN ADVISOR<span>FOR MORE INFORMATION</span>
                        </a>
                        <a href="#" class="btn btn-x1 btn-text">DOWNLOAD FREE KAPLAN BROCHURE</a>
                    </div>
                    <a id="social-act"></a>
                </div>

            </div>

        </div>

    </li>
    <li>
        <div class="link">Social Activities <i class="fa fa-chevron-down"></i></div>
        <div class="submenu social-act ">
            <div class="container">
                <h2> Social Activities </h2>
                <div class="info-tag"><?php print $node_wrapper->field_h2_title->value(); ?> has a dedicated social program, designed to help you explore the city and build friendships, through organised events and activities.</div>
                <div class="row sec-row" >
                    <div class="col-md-4 col-sm-6 col-xs-6 ">
                        <div class="block">
                            <span>MONDAY</span>
                            <img src="<?php echo '/sites/default/files/'.str_replace('public://', '', $social_activity[0]['uri']); ?>"/>
                            <p><?php print $social_activity[0]['title']; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-6 ">
                        <div class="block">
                            <span>TUESDAY</span>
                            <img src="<?php echo '/sites/default/files/'.str_replace('public://', '', $social_activity[1]['uri']); ?>"/>
                            <p><?php print $social_activity[1]['title']; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-6 ">
                        <div class="block">
                            <span>WEDNESDAY</span>
                            <img src="<?php echo '/sites/default/files/'.str_replace('public://', '', $social_activity[2]['uri']); ?>"/>
                            <p><?php print $social_activity[2]['title']; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-6 ">
                        <div class="block">
                            <span>THURSDAY</span>
                            <img src="<?php echo '/sites/default/files/'.str_replace('public://', '', $social_activity[3]['uri']); ?>"/>
                            <p><?php $social_activity[3]['title']; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-6 ">
                        <div class="block">
                            <span>FRIDAY</span>
                            <img src="<?php echo '/sites/default/files/'.str_replace('public://', '', $social_activity[4]['uri']); ?>"/>
                            <p><?php print $social_activity[4]['title']; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-6 ">
                        <div class="block">
                            <span>WEEKEND</span>
                            <img src="<?php echo '/sites/default/files/'.str_replace('public://', '', $social_activity[5]['uri']); ?>"/>
                            <p><?php print $social_activity[5]['title']; ?></p>
                        </div>
                    </div>

                </div>
                <div class="all-week">
                    Number of Social activities per week: 5</div>
                <a id="std-acc"></a>
            </div>
        </div>
    </li>

    <li>
        <div class="link"> Accommodation  <i class="fa fa-chevron-down"></i></div>
        <div class="submenu student-acmo ">
            <div class="container">
                <h2> Student Accommodation in <?php echo $school_name; ?> </h2>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="blocks">

                            <a href="#">
                                <span class="pic"><img src="<?php echo '/'.drupal_get_path('theme',$GLOBALS['theme']); ?>/images/accommodation-residence.jpg"  /></span>
                                <span class="head">Student Residence</span>
                            </a>
                            <p>
                                Student residences are specially selected apartments, hostels, and dorms that provide a fantastic living experience. Enjoy a more independent lifestyle and the social aspects of living with new friends.
                            </p>
                            <div class="list">
                                <ul>
                                    <li>Live with fellow students for more independence      </li>
                                    <li>        Most residences are self-catering      </li>
                                    <li>        Live and socialize with your new friends      </li>
                                    <li>        Single, twin, double, and dorm rooms are available at various locations      </li>
                                </ul>
                                <div class="btn-row">
                                    <a href="#" class="btn btn-sm btn-primary find-out-more">
                                        Find out more    </a>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="blocks">

                            <a href="#">
                                <span class="pic"><img src="<?php echo '/'.drupal_get_path('theme',$GLOBALS['theme']); ?>/images/accommodation-homestay_0.jpg" /></span>
                                <span class="head">Homestay</span>
                            </a>
                            <p>Join our homestay program and live in a British homestay during your time in <?php echo $school_name; ?>. Share meals with your hosts and practice your English outside of school in a relaxed and informal situation.</p>
                            <div class="list">
                                <ul>
                                    <li>Live in a local homestay specially chosen by us </li>
                                    <li> Enjoy breakfasts and dinners with your hosts </li>
                                    <li> Practice English daily in an informal situation </li>
                                    <li> Immerse yourself in a new culture </li>
                                </ul>
                                <div class="btn-row">
                                    <a href="#" class="btn btn-sm btn-primary find-out-more">
                                        Find out more    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </li>

    <a id="contact"></a>
</ul>
</div>
<div class="contact-form form-con-bg" >
    <div class="container">
        <h2 class="page-section-title">Contact us <span>Please provide the following information and we will aim to respond within 48 hours: </span></h2>
        <div class="container-f">
          <?php
          $node = node_load('652');
          webform_node_view($node,'full');
          print theme_webform_view($node->content);

          ?>
        </div>
    </div>
</div>

<div class="modal fade" id="gallery-p" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
        <span class="icon icon-ki-close"></span>
    </a>
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                      <?php $i = 0; foreach($node_wrapper->field_gallery_images->value() as $gallery_images) { ?>
                          <div class="carousel-item <?php if($i == 0) { ?> active <?php } ?>">
                              <img class="d-block w-100" src="<?php echo '/sites/default/files/'.str_replace('public://', '', $gallery_images['uri']); ?>" alt="<?php print $gallery_images['alt']; ?>">
                          </div>
                        <?php $i++; } ?>
                    </div>

                    <ol class="carousel-indicators">
                      <?php $i = 0; foreach($node_wrapper->field_gallery_images->value() as $gallery_images) { ?>
                          <li data-target="#carouselExampleIndicators" data-slide-to="0" <?php if($i == 0) { ?> class="active" <?php } ?>>
                              <img src="<?php echo '/sites/default/files/'.str_replace('public://', '', $gallery_images['uri']); ?>" alt="<?php print $gallery_images['alt']; ?>" />
                          </li>
                        <?php $i++; } ?>


                    </ol>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/sites/all/themes/skytheme/js/jquery.bbq.js"></script>
<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<script>
$(document).ready(function(){
    $(".btn-more").click(function(){
        $(".all-btns").slideToggle();
    });
});
</script>

<script>
$(document).ready(function() {
  $("#toggle").click(function() {
    var elem = $("#toggle").text();
    if (elem == "View more facilities +") {
      //Stuff to do when btn is in the read more state
      $("#toggle").text("View less facilities -");
      $("#text").slideDown();
    } else {
      //Stuff to do when btn is in the read less state
      $("#toggle").text("View more facilities +");
      $("#text").slideUp();
    }
  });
});
</script>

<script>
$(function() {
    var Accordion = function(el, multiple) {
        this.el = el || {};
        this.multiple = multiple || false;

        // Variables privadas
        var links = this.el.find('.link');
        // Evento
        links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
    }

    Accordion.prototype.dropdown = function(e) {
        var $el = e.data.el;
            $this = $(this),
            $next = $this.next();

        $next.slideToggle();
        $this.parent().toggleClass('open');

        if (!e.data.multiple) {
            $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
        };
    }   

    var accordion = new Accordion($('#accordion'), false);
});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script>

$('.tab-content').on('hidden.bs.collapse', function () {
 alert(1)
})
$(".tab-menu-content ul li a[href^='#']").on('click', function(e) {

   // prevent default anchor click behavior
   e.preventDefault();

   // store hash
   var hash = this.hash;

   // animate
   $('html, body').animate({
       scrollTop: $(hash).offset().top
     }, 1000, function(){

       // when done, add hash to url
       // (default click behaviour)
       window.location.hash = hash;
     });

});

$(".footer .link-row .goto-top a[href^='#']").on('click', function(e) {

   // prevent default anchor click behavior
   e.preventDefault();

   // store hash
   var hash = this.hash;

   // animate
   $('html, body').animate({
       scrollTop: $(hash).offset().top
     }, 1000, function(){

       // when done, add hash to url
       // (default click behaviour)
       window.location.hash = hash;
     });

});
</script> 

<script>
(function($) {
$.fn.menumaker = function(options) {  
 var cssmenu = $(this), settings = $.extend({
   format: "dropdown",
   sticky: false
 }, options);
 return this.each(function() {
   $(this).find(".button").on('click', function(){
     $(this).toggleClass('menu-opened');
     var mainmenu = $(this).next('ul');
     if (mainmenu.hasClass('open')) { 
       mainmenu.slideToggle().removeClass('open');
     }
     else {
       mainmenu.slideToggle().addClass('open');
       if (settings.format === "dropdown") {
         mainmenu.find('ul').show();
       }
     }
   });
   cssmenu.find('li ul').parent().addClass('has-sub');
multiTg = function() {
     cssmenu.find(".has-sub").on('click', function() {
  //    $(this).toggleClass('submenu-opened');
   
       if ($(this).find('ul').hasClass('open')) {
         $(this).find('ul').removeClass('open').slideToggle();
         $(".has-sub").find('ul.open').removeClass('open').hide();
       }
       else {
           $(".has-sub").find('ul.open').removeClass('open').hide();
         $(this).find('ul').addClass('open').slideToggle();
       }
          
       
     });
   };
   if (settings.format === 'multitoggle') multiTg();
   else cssmenu.addClass('dropdown');
   if (settings.sticky === true) cssmenu.css('position', 'fixed');
resizeFix = function() {
  var mediasize = 1000;
     if ($( window ).width() > mediasize) {
       cssmenu.find('ul').show();
     }
     if ($(window).width() <= mediasize) {
       cssmenu.find('ul').hide().removeClass('open');
     }
   };
   resizeFix();
   return $(window).on('resize', resizeFix);
 });
  };
})(jQuery);

(function($){
$(document).ready(function(){
$("#cssmenu").menumaker({
   format: "multitoggle"
});
});
})(jQuery);

</script> 
<script>
$(document).ready(function() {

// Gets the video src from the data-src on each button
$('.form-type-checkbox label').removeClass('option');
var $videoSrc;  
$('.video-btn').click(function() {
    $videoSrc = $(this).data( "https://www.youtube.com/watch?v=IadsLclBOS8" );
});
console.log($videoSrc);

  
  
// when the modal is opened autoplay it  
$('#myModal').on('shown.bs.modal', function (e) {
    
// set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
$("#video").attr('src',$videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1" ); 
})
  
  
// stop playing the youtube video when I close the modal
$('#myModal').on('hide.bs.modal', function (e) {
    // a poor man's stop video
    $("#video").attr('src',$videoSrc); 
}) 
// document ready  
});
</script> 
<script>
$('.panel-heading a').click(function() {
    $('.panel-heading').removeClass('active');
    
    //If the panel was open and would be closed by this click, do not active it
    if(!$(this).closest('.sub-catg').find('.collapse ').hasClass('show'))
        $(this).parents('.panel-heading').addClass('active');
 });
</script>