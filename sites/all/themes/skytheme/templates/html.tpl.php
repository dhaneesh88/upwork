<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700,800" rel="stylesheet">
  <?php
  print $styles;
  global $base_url;
  ?>
  <link rel="shortcut icon" href="/sites/all/themes/skytheme/images/k-icon.png" type="image/vnd.microsoft.icon" />
  <link href="/sites/all/themes/skytheme/css/jquery.atAccordionOrTabs.css" rel="stylesheet" type="text/css">
  <style type="text/css">
    .pos-rel{
      position: relative;
    }
  </style>
  <script src="/sites/all/themes/skytheme/js/jquery/jquery.min.js"></script>
  <script src="/sites/all/themes/skytheme/js/bootstrap.bundle.min.js"></script>
  <?php print $scripts; ?>
</head>
<body>
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
  <?php
  if (isset($footer_code)): print $footer_code; endif;
  ?>
  <script type="text/javascript" src="/sites/all/themes/skytheme/js/scroll.js"></script>
  <script type="text/javascript" src="/sites/all/themes/skytheme/js/animate.min.js"></script>
  <script type="text/javascript" src="/sites/all/themes/skytheme/js/jquery.easing.min.js.js"></script>
  <script type="text/javascript" src="/sites/all/themes/skytheme/js/bootstrap-tabcollapse.js"></script>

<script>

$('.tab-content').on('hidden.bs.collapse', function () {
 alert(1)
})
$(".tab-menu-content ul li a[href^='#']").on('click', function(e) {

   // prevent default anchor click behavior
   e.preventDefault();

   // store hash
   var hash = this.hash;

   // animate
   $('html, body').animate({
       scrollTop: $(hash).offset().top
     }, 1000, function(){

       // when done, add hash to url
       // (default click behaviour)
       window.location.hash = hash;
     });

});

$(".footer .link-row .goto-top a[href^='#']").on('click', function(e) {

   // prevent default anchor click behavior
   e.preventDefault();

   // store hash
   var hash = this.hash;

   // animate
   $('html, body').animate({
       scrollTop: $(hash).offset().top
     }, 1000, function(){

       // when done, add hash to url
       // (default click behaviour)
       window.location.hash = hash;
     });

});
</script> 
<script>
(function($) {
$.fn.menumaker = function(options) {  
 var cssmenu = $(this), settings = $.extend({
   format: "dropdown",
   sticky: false
 }, options);
 return this.each(function() {
   $(this).find(".button").on('click', function(){
     $(this).toggleClass('menu-opened');
     var mainmenu = $(this).next('ul');
     if (mainmenu.hasClass('open')) { 
       mainmenu.slideToggle().removeClass('open');
     }
     else {
       mainmenu.slideToggle().addClass('open');
       if (settings.format === "dropdown") {
         mainmenu.find('ul').show();
       }
     }
   });
   cssmenu.find('li ul').parent().addClass('has-sub');
multiTg = function() {
     cssmenu.find(".has-sub").on('click', function() {
  //    $(this).toggleClass('submenu-opened');
   
       if ($(this).find('ul').hasClass('open')) {
         $(this).find('ul').removeClass('open').slideToggle();
     $(".has-sub").find('ul.open').removeClass('open').hide();
       }
       else {
       $(".has-sub").find('ul.open').removeClass('open').hide();
         $(this).find('ul').addClass('open').slideToggle();
       }
        
     
     });
   };
   if (settings.format === 'multitoggle') multiTg();
   else cssmenu.addClass('dropdown');
   if (settings.sticky === true) cssmenu.css('position', 'fixed');
resizeFix = function() {
  var mediasize = 1000;
     if ($( window ).width() > mediasize) {
       cssmenu.find('ul').show();
     }
     if ($(window).width() <= mediasize) {
       cssmenu.find('ul').hide().removeClass('open');
     }
   };
   resizeFix();
   return $(window).on('resize', resizeFix);
 });
  };
})(jQuery);

(function($){
$(document).ready(function(){
  if($('.demo').length) {
    $('.demo').accordionortabs();
  }
  
$("#cssmenu").menumaker({
   format: "multitoggle"
});
});
})(jQuery);

</script> 
<script>
$(document).ready(function() {

// Gets the video src from the data-src on each button

var $videoSrc;  
$('.video-btn').click(function() {
    $videoSrc = $(this).data( "https://www.youtube.com/watch?v=IadsLclBOS8" );
});
console.log($videoSrc);

  
  
// when the modal is opened autoplay it  
$('#myModal').on('shown.bs.modal', function (e) {
    
// set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
$("#video").attr('src',$videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1" ); 
})
  
  
// stop playing the youtube video when I close the modal
$('#myModal').on('hide.bs.modal', function (e) {
    // a poor man's stop video
    $("#video").attr('src',$videoSrc); 
}) 
// document ready  
});
</script> 
<script>
  $('.carousel').carousel();
$('.panel-heading a').click(function() {
    $('.panel-heading').removeClass('active');
    
    //If the panel was open and would be closed by this click, do not active it
    if(!$(this).closest('.sub-catg').find('.collapse ').hasClass('show'))
        $(this).parents('.panel-heading').addClass('active');
 });
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>