<?php

global $user;

?>
<div class="outer-wrapper " id="main-wrapper">
    <header class="fixed-top">
        <nav class="navbar navbar-expand-lg navbar-dark ">
            <div class="container-fluid ">
                <div class="logo-d">
                    <a class="navbar-brand" href="#">
                        <img src="/sites/all/themes/skytheme/images/logo.png" />
                    </a>
                </div>
                <div class="navbar-collapse">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a href="#" id="cta-contact-us" title="Contact an Advisor">
                                <span class="icon icon-ki-email"></span>
                                <span class="lnk-txt" >Contact an Advisor</span>
                            </a>
                        </li>
                        <li class="nav-item"> <a href="#" id="cta-contact-us" title="Get a Quote">
                            <span class="icon icon-ki-calculator"></span> <span class="lnk-txt" >Get a Quote</span></a></li><li class="nav-item"> <a href="#" id="cta-contact-us" title="Download Brochure"> <span class="icon icon-ki-brochure"></span> <span class="lnk-txt" >Download Brochure</span></a></li><li class="nav-item"> <a href="#" id="cta-contact-us" title="CTelephone number to market"> <span class="icon icon-ki-faci-phone"></span><span class="lnk-txt" >+44 (0) 20 7045 5000</span></a> </li><li class="nav-item site-search"> <a href="#" id="cta-contact-us" title="Contact an Advisor" data-toggle="modal" data-target="#searchModal"> <span class="icon icon-ki-search"></span> </a> </li><li class="nav-item"> <a href="#" id="cta-contact-us" title="Contact an Advisor"> <span class="icon icon-ki-world"></span> </a> </li>
                    </ul>
                </div>
            </div>
        </nav>
        <?php //print str_replace(array('<br />', '<br/>', '<br>'), '', render($page['nav_menu'])); ?>
        <?php include(drupal_get_path('theme', 'skytheme').'/menu.tpl.php'); ?>
    </header>
    <div class="content-section <?php echo $add_extra_wrap_class; ?> <?php if($user->uid) { echo $adjst_top_space; } ?>" id="<?php echo $add_wrap_id ?>">
        
      <?php
        // We hide the comments and links now so that we can render them later.
        hide($content['comments']);
        hide($content['links']);
      ?>
      <?php if (strpos(current_path(), 'newsletter') !== false) { ?>
        <div class="new-york-pg us-course">
            <div class="container top-container">
                <div class="eng-school">
                    <?php  print render($page['content']); ?>
                </div>
            </div>
        </div> 
      <?php } else { 
        print render($page['content']);
        } ?>

        <div class="footer">
            <?php print render($page['page_bottom']); ?>
            <?php print render($page['footer_top']); ?>
            <?php print render($page['footer']); ?>
        </div>
    </div>
</div>
