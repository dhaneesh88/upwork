<?php

$term = taxonomy_term_load(arg(2));
$term_id = $term->tid;
$parent = array_shift(taxonomy_get_parents($term_id));

$query = new EntityFieldQuery();
$query->entityCondition('entity_type', 'node')
  ->entityCondition('bundle', 'school');
  $query->fieldCondition('field_country', 'tid', $term_id);
$result = $query->execute();

$google_map_highlight = '';
$google_map_tooltip = '';
$google_map_center = array();
if (!empty($result['node'])) {
    $nids = array_keys($result['node']);
    $node_schools = node_load_multiple($nids);
    $i = 0;
    foreach ($node_schools as $key => $node_school) {

        $node_school_wrapper = entity_metadata_wrapper('node', $node_school);
        $school_location = str_replace('English school in ', '', $node_school_wrapper->field_h1_title->value());
        $school_location_address = str_replace(array("\n", "\r"), ' ', $node_school_wrapper->field_location_description->value());
        $school_images = $node_school_wrapper->field_gallery_images->value();
        $school_url = drupal_get_path_alias('node/'.$node_school_wrapper->nid->value());
        if(count($school_images) > 0 ) {
         $school_image = file_create_url($school_images[0]['uri']);   
        } else {
            $school_image = 'https://cdn.kaplaninternational.com/sites/default/files/styles/190x125/public/city/photos/boston-fenway-and-city-page.jpg?itok=5oj0-_E_';
        }
       // $tool_tip_content = '<div class="info_content"><h3>'.$school_location.'</h3><p>'.$school_location_address.'</p></div>';

       $tool_tip_content = '<div class="gm-style-iw"><div><div style="overflow: auto;"><div class="map-popup"><h4 class="popup-title"><a href="/'.$school_url.'">1 Kaplan school in '.$school_location.'</a></h4><div class="popup-image"><a href="/united-states/boston"><img class="img-responsive" src="'.$school_image.'" width="190" height="125" alt=""></a></div><a class="btn btn-primary" href="/'.$school_url.'">Find out more</a></div></div></div></div>'; 


        
        $school_coordinates = $node_school_wrapper->field_coordinates->value();
        if($i == 0) {
            $google_map_highlight .= "['".$school_location_address."',".
                $school_coordinates[0].",".$school_coordinates[1].", '".$school_location."']";
            $google_map_center[0] = bcdiv($school_coordinates[0], 1, 1);
            $google_map_center[1] = bcdiv($school_coordinates[1], 1,1);

            $tool_tip .= "['".$tool_tip_content."']";

        } else {
            $google_map_highlight .= ", ['".$school_location_address."',".
                $school_coordinates[0].",".$school_coordinates[1].", '".$school_location."']";
                $tool_tip .= ", ['".$tool_tip_content."']";
        }
        $i++;
    }
}
$node_wrapper = entity_metadata_wrapper('taxonomy_term', $term_id);
$features = $node_wrapper->field_features_ticked->value();
$video_url = $node_wrapper->field_video->value();


?>
<div class="breadcrumb-wrapper">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/destinations">Destinations and Schools</a></li>
            <?php if(count($parent) > 0) { ?>
            <li class="breadcrumb-iteme">
                <a href="<?php print url('taxonomy/term/'.$parent->tid); ?>"><?php print $parent->name; ?></a>
            </li>
            <li class="breadcrumb-iteme active">
                <span href="<?php print url('taxonomy/term/'.$term->tid); ?>"><?php print $term->name; ?></span>
            </li>
            <?php } else { ?>
            <li class="breadcrumb-iteme active">
                <span href="#"><?php print $term->name; ?></span>
            </li>
            <?php } ?>
        </ol>
</div>
<div class="new-york-pg us-course">
    <div class="eng-course">
        <div class="container">
            <h2 class="page-header"><?php print $node_wrapper->field_h1_title->value(); ?></h2>
            <div class="subline-tag"><?php print $node_wrapper->field_h2_title->value(); ?></div>
        </div>
        <div class="tab-menus navbar-collapse" >
            <div class="container">
                <ul>
                    <li><a class="js-scroll-trigger" href="#over-us">Overview</a></li>
                    <li><a class="js-scroll-trigger" href="#s-location">School locations</a></li>
                    <li><a class="js-scroll-trigger" href="#school-list">School list</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container top-container">
        <div class="top-section">
            <div class="row-section">
                <div class="row">
                    <div class="col-sm-6 col-1-dv">
                      <?php print views_embed_view('destinations_slider', 'default', $term_id); ?>
                    </div>
                    <div class="col-sm-6 col-2-dv">
                        <div class="video-view">
                            <iframe width="560" height="100%" src="<?php if($video_url['video_url']) { print $video_url['video_url']; } else { ?>https://www.youtube.com/embed/L1shgss2eBo <?php } ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="learn-eng" id="over-us">
            <div class="row">
                <div class="col-xs-12 col-sm-8 ">
                    <div class="sec-left" >
                        <h3><?php print $node_wrapper->field_h1_title->value(); ?></h3>
                        <p><?php print $node_wrapper->description->value(); ?></p>
                        <ul>
                            <?php foreach($features as $feature) { ?>
                            <li><?php print $feature; ?></li>
                            <?php } ?>
                        </ul>
                        <div class="btn-container">
                            <a href="#" title="FOR MORE INFORMATION" class="btn btn-x1 btn-default">
                                CONTACT AN ADVISOR<span>FOR MORE INFORMATION</span>
                            </a>
                            <a href="#" class="btn btn-x1 btn-text">DOWNLOAD FREE KAPLAN BROCHURE</a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 ">
                  <?php print views_embed_view('schools_at_this_destination', 'block_3', $term_id); ?>
                </div>

            </div>
        </div>

        <div class="eng-school map-wrapper" >
            <div class="container top-container">
                <div class="row">
                    <div class="col-sm-12" id="s-location">
                        <h3>Destinations in <?php print $node_wrapper->name->value(); ?></h3>
                        <div class="ny-map">
                            <div id="map" style=" height: 100%; width: 100%; margin: 0px; padding: 0px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="eng-school">
        <div class="container top-container">
            <div class="row" id="school-list">
              <?php print views_embed_view('schools_at_this_destination', 'default', $term_id); ?>
            </div>
        </div>
    </div>

    <?php if(count($node_wrapper->field_ttd_images->value()) > 0) { ?>
    <div class="thinks-do">
        <div class="container top-container">
            <div class="row ">
                <div class="col-md-12">
                    <h3>Things to Do and See</h3>
                </div>
            </div>
            <div class="row row-pd-rm">
                <?php foreach ($node_wrapper->field_ttd_images->value() as $key => $ttd_images) { ?>
                    <div class="col-4 col-sm-4 col-pd">
                        <div class="block">
                            <img src="<?php print file_create_url($ttd_images['uri']); ?>" alt="<?php print $ttd_images['title']; ?>"/>
                        </div>
                    </div>
                <?php } ?>
            </div>
           
            <div class="row ">
                <div class="col-md-12">
                    <div class="list">
                        <div class="panel-group" id="accordion" >
                            <?php if($node_wrapper->field_history->value() != '') { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <span class="icon icon-ki-act-history"></span>
                                        <a  data-toggle="collapse" href="#collapseOne" >
                                            <span class="more-less icon icon-ki-arrow-down"></span>
                                                History
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse body-block">
                                    <div class="panel-body">
                                    <?php print $node_wrapper->field_history->value(); ?>
                                    </div>
                                </div>
                            </div>
                            <?php } ?> 
                            <?php if($node_wrapper->field_shopping->value() != '') { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <span class="icon icon-ki-act-shopping"></span>
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                <span class="more-less icon icon-ki-arrow-down"></span>
                                                    Shopping
                                            </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse body-block" >
                                    <div class="panel-body">
                                        <?php print $node_wrapper->field_shopping->value(); ?> 
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if($node_wrapper->field_arts_theatre->value() != '') { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                    <span class="icon icon-ki-act-art"></span>
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" >
                                            <span class="more-less icon icon-ki-arrow-down"></span>
                                          Arts & Theatre
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse body-block" >
                                    <div class="panel-body">
                                     <?php print $node_wrapper->field_arts_theatre->value(); ?>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if($node_wrapper->field_nightlife->value() != '') { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingfoure">
                                    <h4 class="panel-title">
                                    <span class="icon icon-ki-act-nightlife"></span>
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefoure">
                                           <span class="more-less icon icon-ki-arrow-down"></span>
                                          Nightlife
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapsefoure" class="panel-collapse collapse body-block">
                                    <div class="panel-body">
                                    <?php print $node_wrapper->field_nightlife->value(); ?>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if($node_wrapper->field_food_drink->value() != '') { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingfive">
                                    <h4 class="panel-title">
                                    <span class="icon icon-ki-act-restaurant"></span>
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive">
                                        <span class="more-less icon icon-ki-arrow-down"></span>Food & Drink</a>
                                    </h4>
                                </div>
                                <div id="collapsefive" class="panel-collapse collapse body-block">
                                    <div class="panel-body">
                                        <?php print $node_wrapper->field_food_drink->value(); ?>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if($node_wrapper->field_sports->value() != '') { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="sportssix">
                                    <h4 class="panel-title">
                                    <span class="icon icon-ki-act-sports"></span>
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#sports" >
                                        <span class="more-less icon icon-ki-arrow-down"></span>Sports</a>
                                    </h4>
                                </div>
                                <div id="sports" class="panel-collapse collapse body-block" >
                                    <div class="panel-body">
                                        <?php print $node_wrapper->field_sports->value(); ?>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if($node_wrapper->field_outdoor_activities->value() != '') { ?>
                            <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="outdoorseven">
                                        <h4 class="panel-title">
                                        <span class="icon icon-ki-act-outdoor"></span>
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#outdoor">
                                            <span class="more-less icon icon-ki-arrow-down"></span>Outdoor Activities</a>
                                        </h4>
                                    </div>
                                    <div id="outdoor" class="panel-collapse collapse body-block" >
                                        <div class="panel-body">
                                            <?php print $node_wrapper->field_outdoor_activities->value(); ?>
                                        </div>
                                    </div>
                                </div>   
                                <?php } ?>
                                <?php if($node_wrapper->field_wildlife->value() != '') { ?>
                                <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="wildlifeighte">
                                            <h4 class="panel-title">
                                            <span class="icon icon-ki-act-nature"></span>
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#wildlife" >
                                                <span class="more-less icon icon-ki-arrow-down"></span>Wildlife</a>
                                            </h4>
                                        </div>
                                        <div id="wildlife" class="panel-collapse collapse body-block" >
                                            <div class="panel-body">
                                                <?php print $node_wrapper->field_wildlife->value(); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>

</div>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfgD9OKE8ywNjeJxXOsn9EKumSZNK8FDQ&callback=initialize"
  type="text/javascript"></script>
<script>

function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var myLatLng = new google.maps.LatLng(<?php print_r($google_map_center[0]); ?>, <?php print_r($google_map_center[1]); ?>);
    var mapOptions = {
        zoom: 4,
        center: myLatLng,
        mapTypeId: google.maps.MapTypeId.MAP
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map"), mapOptions);


    function CustomMarker(latlng,  map, location) {
     this.latlng_ = latlng;
     this.location = location;
     // Once the LatLng and text are set, add the overlay to the map.  This will
     // trigger a call to panes_changed which should in turn call draw.
     this.setMap(map);
   }

   CustomMarker.prototype = new google.maps.OverlayView();

   CustomMarker.prototype.draw = function() {
     var me = this;

     // Check if the div has been created.
     var div = this.div_;
     if (!div) {
       // Create a overlay text DIV
       div = this.div_ = document.createElement('DIV');
       // Create the DIV representing our CustomMarker
       div.style.position = 'absolute';
       div.style.paddingLeft = '0px';
       div.style.cursor = 'pointer';
       div.innerHTML = '<div class="tooltip-primary tooltip top tooltip-static in"><div class="tooltip-arrow"></div><div class="tooltip-inner">'+this.location+' (1)</div></div>';

       google.maps.event.addDomListener(div, "click", function(event) {
         google.maps.event.trigger(me, "click");
       });

       // Then add the overlay to the DOM
       var panes = this.getPanes();
       panes.overlayImage.appendChild(div);
     }

     // Position the overlay 
     var point = this.getProjection().fromLatLngToDivPixel(this.latlng_);
     if (point) {
       div.style.left = point.x + 'px';
       div.style.top = point.y + 'px';
     }
   };

   CustomMarker.prototype.remove = function() {
     // Check if the overlay was on the map and needs to be removed.
     if (this.div_) {
       this.div_.parentNode.removeChild(this.div_);
       this.div_ = null;
     }
   };

   CustomMarker.prototype.getPosition = function() {
    return this.latlng_;
   };



    // Multiple Markers
    var markers = [<?php print_r($google_map_highlight); ?>];
        
                        
    // Info Window Content
    var infoWindowContent = [<?php print_r($tool_tip); ?>];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);

        myLatlng = new google.maps.LatLng(markers[i][1], markers[i][2]);
        marker = new CustomMarker(myLatlng, map, markers[i][3]);
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));
    }    
}
</script>
 