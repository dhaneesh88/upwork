<?php

$term = taxonomy_term_load(arg(2));
$term_id = $term->tid;

$vocabulary = taxonomy_vocabulary_machine_name_load('faqs');
$faq_terms = entity_load('taxonomy_term', FALSE, array('vid' => $vocabulary->vid));

$kalpan_faqs = array();
$kalpan_top_faqs = array();
$current_faqs_section = '';
$query = new EntityFieldQuery();
$query->entityCondition('entity_type', 'school_objects')
  ->entityCondition('bundle', 'faqs');
  if(isset($term_id) && $term_id != '') {
    $query->fieldCondition('field_faq_type', 'tid', $term_id);
  }
  
$result = $query->execute();

if (!empty($result['school_objects'])) {
  $nids = array_keys($result['school_objects']);
  $entity_faqs = entity_load('school_objects', $nids);
  foreach ($entity_faqs as $faq) {
    $wrapper = entity_metadata_wrapper('school_objects', $faq->id, array('bundle' => 'faqs'));
    if($wrapper->field_section->value()) {
      $faq_body = $wrapper->field_faq_body->value();
      $kalpan_faqs[$wrapper->field_section->value()][] = array('title' => $wrapper->title->value(), 'body' => $faq_body['value'], 'update_date' => $faq->changed);
    }
  }
}
$query = new EntityFieldQuery();
$query->entityCondition('entity_type', 'school_objects')
    ->entityCondition('bundle', 'faqs')
    ->propertyOrderBy('changed', 'DESC')
    ->range(0,10);
$result = $query->execute();
if (!empty($result['school_objects'])) {
  $nids = array_keys($result['school_objects']);
  $entity_faqs = entity_load('school_objects', $nids);
    foreach ($entity_faqs as $faq) {
      $kalpan_top_faqs[] = $faq->title;
    }
}
?>

<div class="breadcrumb-wrapper">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/faq-categories/general-information">FAQs and Support</a></li>
            <li class="breadcrumb-iteme"><a href="#">Studying English abroad</a></li>

        </ol>
    </nav>
</div>
<div class="eng-course-faq">
    <div class="eng-course">
        <div class="container">
            <h2 class="page-header">FAQs - English Courses</h2>
        </div>
    </div>
    <div class="faq-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-8 content-left">

                    <div class=" course-list">
                        <div class="text-summry">
                            <h3> English Courses</h3>
                            <p>Studying abroad is a big decision to make, and it’s important that you have all the information you want before you start your journey with us. Our FAQs are divided into categories, and should provide you will answers to all your queries.</p>
                            <p>If you cannot find an answer to your question, please <a href="#">contact a student advisor.</a></p>
                        </div>

                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <?php $i = 1; foreach ($kalpan_faqs as $faqs_section => $kalpan_faq) {
                              if ($current_faqs_section != $faqs_section) { ?>
                                  <h4><?php print $faqs_section; ?></h4>
                             <?php } ?>
                              <?php foreach ($kalpan_faq as $faq) { ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading scroll" role="tab" id="heading-<?php print $i; ?>">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#li-<?php print $i; ?>" aria-expanded="true" aria-controls="collapseOne">
                                                <span class="more-less icon icon-ki-arrow-down"></span>
                                                <?php print $faq['title']; ?>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="li-<?php print $i; ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading-<?php print $i; ?>">
                                        <div class="panel-body">
                                            <?php print $faq['body']; ?>
                                            <div class="date"> Last updated: <?php print date("d M Y", $faq['update_date']); ?> </div>
                                        </div>
                                    </div>
                                </div>
                              <?php $i++; } ?>
                            <?php  $current_faqs_section = $faqs_section;
                            }
                            ?>
                            <div class="">
                                <div class="view-bottom">
                                    If your question is not answered here, please contact <a href="#">a student advisor</a>.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 content-right">
                    <div class="list-right">
                        <div class="topic-list">
                            <h4>Browse by topic</h4>
                            <ul>
                                <?php foreach($faq_terms as $faq_term) { ?>
                                    <li>
                                        <span class="icon icon-ki-faq-general"></span>
                                        <a href="<?php print url('taxonomy/term/'.$faq_term->tid); ?>"><?php print $faq_term->name; ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>

                        <div class="top-list">
                            <h4> FAQ - Top 10</h4>
                            <ol>
                                <?php foreach($kalpan_top_faqs as $top_faqs) { ?>
                                <li><a href="#"><?php print $top_faqs; ?></a></li>
                                <?php } ?>
                            </ol>
                        </div>

                        <div class="help-col">
                            <h4>How can we help?</h4>

                            <p>Our advisors will help you choose the right course to suit your academic and professional goals.</p>
                            <ul>
                                <li> Accommodation</li>
                                <li> Visa information</li>
                                <li>Travel arrangements</li></ul>

                            <p>If you would like to speak with an advisor, please call: </p>
                            <p class="contact"><span class="icon icon-ki-phone"></span><span style="line-height: 1.42857143;">UK: +44 (0)20 7045 5000</span></p>
                            <p class="contact"><span class="icon icon-ki-phone"></span><span style="line-height: 1.42857143;">USA:1-888-744-3046</span></p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $( document ).ready(function() {

            $(".panel").on("show.bs.collapse hide.bs.collapse", function(e) {
                $("div.panel").removeClass('active');
                $("div.panel-collapse").removeClass('show');
                $("span.more-less").addClass("icon-ki-arrow-down");
                if (e.type=='show'){
                    $(this).addClass('active');
                }else{
                    $(this).removeClass('active');
                }
            });
        });
    </script>
    <script>
        function toggleIcon(e) {
            $(e.target)
                .prev('.panel-heading')
                .find(".more-less")
                .toggleClass('icon-ki-arrow-down icon-ki-minus');
        }
        $('.panel-group').on('hidden.bs.collapse', toggleIcon);
        $('.panel-group').on('shown.bs.collapse', toggleIcon);
    </script>
    <script>
        $('#accordion').on('shown.bs.collapse', function () {

            var panel = $(this).find('.active');
            $('html, body').animate({
                scrollTop: panel.offset().top-120
            }, 500);

        });
    </script>