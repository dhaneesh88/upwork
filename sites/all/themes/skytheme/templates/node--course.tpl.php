<?php
/**
* course template theme
*
*/

//HOME>>ALL ENGLISH COURSES>>TOP LEVEL COURSE NAME>>COURSE NAME
$language = $node->language;
$node_wrapper = entity_metadata_wrapper('node', $node);
$featured_contents = $node_wrapper->field_features_ticked->value();
$course_titles = $node_wrapper->field_cs_title->value();
$course_hours = $node_wrapper->field_cs_hours->value();
$course_descriptions = $node_wrapper->field_cs_description->value();
$course_destinations = $node_wrapper->field_country->value();
$country_icons = array('australia' => 'au-f',
  'canada' => 'ca-f',
  'ireland' => 'ir-f',
  'new_zealand' => 'nz-f',
  'united_kingdom' => 'uk-f',
  'united_states' => 'us-f',
  );

$query = new EntityFieldQuery();
$query->entityCondition('entity_type', 'school_objects')
  ->entityCondition('bundle', 'testimonials')
  ->range(0, 4);
$result = $query->execute();
if (!empty($result['school_objects'])) {
  $nids = array_keys($result['school_objects']);
  $entity_testimonails = entity_load('school_objects', $nids);
}
$top_level_course = $node_wrapper->field_course_type->value();
$other_interest_course_img = $node_wrapper->field_oc_image->value();
$other_interest_course_title = $node_wrapper->field_oc_title->value();
$other_interest_course_desc = $node_wrapper->field_oc_desc->value();
$other_interest_course_link = $node_wrapper->field_oc_link->value();
?>
<style type="text/css">
    #myCarousel{
        margin: 60px auto 0 auto;
        padding: 0 10px;
        height: auto;
    }
</style>


<div class="breadcrumb-wrapper">
  <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/english-courses">All English Courses</a></li>
    <?php if($top_level_course[0]->name) { ?>
      <li class="breadcrumb-iteme">
        <a href="<?php print url('taxonomy/term/'.$top_level_course[0]->tid); ?>"><?php print $top_level_course[0]->name; ?></a>
      </li>
      <?php } ?>
    <li class="breadcrumb-iteme active">
      <span><?php print $node_wrapper->title->value(); ?></span>
    </li>
  </ol>
</div>
<div class="banner-top banner-full">
  <div class="container-fluid">
    <h2><?php print $node_wrapper->field_h1_title->value(); ?></h2>
    <div class="head-subtitle"><?php print $node_wrapper->field_subtitle->value(); ?></div>
  </div>
</div>
<a id="section-region"></a>
<div class="tab-menu-content">
  <div class="container">
    <ul class="smooth-scroll list-unstyled">
      <li><a class="scroll" href="#section-region">Overview</a></li>
      <li><a class="scroll" href="#program">Program Structure</a></li>
      <li><a class="scroll" href="#course">Where to study</a></li>
      <li><a class="scroll" href="#choose">Why choose our courses</a></li>
      <li><a class="scroll" href="#contact">Contact</a></li>
    </ul>
  </div>
</div>
<div id="">
      <div class="region-container">
        <div class="container">
          <div class="row" >
            <h2><?php print $node_wrapper->field_h1_title->value(); ?></h2>
          </div>
          <div class="row">
            <div class="col-md-8 section-left">
              <div class="left">
              	<?php print $node_wrapper->field_main_body->value(); ?>
                <!-- <p class="text-p"> Our Semi-Intensive English course is a semi-intensive programme that allows you to balance classroom tuition with structured study and free time activities. You will improve your English language ability at a speed you feel comfortable with, while having time to enjoy your destination. </p>
                <p class="text-p"> This course is available at all levels from Elementary to Advanced and you will be placed in a class which is suitable for your level. The course focuses on the four main aspects of language: reading, writing, listening and speaking. You'll also learn vocabulary and grammar, and how to speak in formal and informal discussions and strategies. </p>
                <div class="grey-box">
                  <div class="content"><img src="images/gap-badge-en.svg" />
                    <h4>GUARANTEE YOUR PROGRESS</h4>
                    <p>If you switch to an Intensive course of 10 weeks or more, we can guarantee you will improve your English by at least one level of fluency in one of our US, UK, Canada or Ireland schools. If you don't, we'll even give you an extra four weeks of tuition absolutely free. <a href="#" target="_blank">Terms and conditions apply</a></p>
                  </div>
                </div> -->
                <div class="listing-box">
                  <ul>
                   <?php
                        foreach($featured_contents as $featured_content) {
                           echo '<li>'.$featured_content.'</li>';
                	} ?>
                  </ul>
                </div>
                <div class="blue-btn"><a href="#m" title="FOR MORE INFORMATION">CONTACT AN ADVISOR<span>FOR MORE INFORMATION</span></a></div>
                <div class="downloads-lnk"><a href="#" >DOWNLOAD FREE KAPLAN BROCHURE</a></div>
              </div>
            </div>
            <div class="col-md-4 section-right">
              <div class="right">
                <h3 >Key facts</h3>
                <ul>
                  <li > <span class="head">Entry level</span> <span class="rigth-list"> <?php print $node_wrapper->field_entry_level->value(); ?>
                    <div class="my-entry-level"> <a href="#" target="_blank"> What is my English level <span class="icon icon-ki-question"></span> </a> </div>
                    </span> </li>
                  <li > <span class="head">Minimum age</span> <span class="rigth-list"><?php print $node_wrapper->field_min_age->value(); ?></span> </li>
                  <li > <span class="head"> Lesson hours</span> <span class="rigth-list"><?php print $node_wrapper->field_lesson_hours->value(); ?></span> </li>
                  <li > <span class="head"> Class size</span> <span class="rigth-list"><?php print $node_wrapper->field_class_size->value(); ?></span> </li>
                  <li > <span class="head">Timetable</span> <span class="rigth-list"><?php print $node_wrapper->field_timetable->value(); ?></span> </li>
                </ul>
                <div class="course">
                  <div class="head">Course structure<strong></strong> </div>
                  <div class="listing-box">
                    <ul >
                    	<?php
                        foreach($course_titles as $course_title) {
                           echo '<li>&nbsp;'.$course_title.'</li>';
                		} ?>
                    	
                    </ul>
                    <div class="find-lnk"><a href="#" >Find out more</a></div>
                  </div>
                </div>
                <div class="find-text">Find out more about this course </div>
                <a class="btn fact-lnk" target="_blank" href="#">Fact Sheet</a> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    

    <div class="testimonials-section" id="sectiontes-timonials">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
              <!-- Carousel indicators -->
              <ol class="carousel-indicators">
                <?php for($i = 0; $i < count($entity_testimonails); $i++) { ?>
                  <li data-target="#myCarousel" data-slide-to="<?php print $i; ?>" <?php if($i == 0) { ?> class="active" <?php } ?>></li>
                 <?php } ?>
              </ol>
              <!-- Wrapper for carousel items -->
              <div class="carousel-inner">
                <?php $i = 0; foreach($entity_testimonails as $testimonails) {
                    $testimonail_wrapper = entity_metadata_wrapper('school_objects', $testimonails->id, array('bundle' => 'testimonials'));
                    $student_img = $testimonail_wrapper->field_student_image->value();
                ?>
                    <div class="item carousel-item <?php if($i == 0) { ?> active <?php } ?>">
                      <div class="namecol">
                        <div class="img-box">
                          <img src="/sites/default/files/<?php print $student_img['filename']; ?>" alt="<?php print $testimonail_wrapper->field_student_name->value(); ?>">
                        </div>
                          <p class="overview"><?php print $testimonail_wrapper->field_student_name->value(); ?> , <span> <?php print $testimonail_wrapper->field_study_details->value(); ?> </span></p>
                      </div>
                      <p class="testimonial"><?php print $testimonail_wrapper->field_testimonial_body->value(); ?></p>
                    </div>
                 <?php $i++; } ?>
              </div>
              <!-- Carousel controls --> 
              <a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev"> <i class="fa fa-angle-left"></i> </a> <a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next"> <i class="fa fa-angle-right"></i> </a> </div>
            <a id="program"></a> </div>
          <div class="col-md-6">
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                    <!-- 16:9 aspect ratio -->
                    <div class="embed-responsive embed-responsive-16by9">
                      <video width="400" controls>
                        <source src="https://youtu.be/668nUCeBHyY"  type="video/mp4">
                        <!-- <source src="https://www.youtube.com/embed/6uhRxK_EOm4?start=180" type="video/ogg">--> 
                        Your browser does not support HTML5 video. </video>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="right-img"  data-toggle="modal" data-target="#myModal"> <img src="/sites/all/themes/skytheme/images/ce669f2b6444ae8bf48e50bfe2f9c6902bf562eb.jpg" />
              <div class="icon" ><img src="/sites/all/themes/skytheme/images/arrow.png" width="127" height="81" /> <span>2:47</span></div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div>
      <div class="program-container" >
        <div class="container">
          <div class="row">
            <h2>Program Structure</h2>
          </div>
          <div class="row">
            <div class="content-box">
              <?php for($i = 0; $i < 2; $i++) {
                if(isset($course_titles[$i])) {
                  ?>
                    <div class="block <?php if($i != 0) { ?> plus <?php } ?>">
                        <div class="title">
                            <div class="lg-hd"><?php print $course_hours[$i]; ?></div>
                            <div class="text-head"><?php print $course_titles[$i]; ?></div>
                        </div>
                        <p><?php print $course_descriptions[$i]; ?></p>
                    </div>
                <?php } } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="offer-course">
      <h3>Which schools offer this course</h3>
      <div class="container">
        <div class="tab-container" >
          <ul class="demo">
            <?php
            foreach($course_destinations as $destination) { ?>
              <li>
                  <a class="">
                      <span class="flag-icons <?php print $country_icons[str_replace(' ', '_', strtolower($destination->name))]?>"><!-- icon --></span>
                      <span class="block text-label"><?php echo $destination->name; ?></span>
                      <span class="down-arrow"></span>
                  </a>
              <section>
                <p><?php $des_states = taxonomy_get_children($destination->tid, $destination->vid);
                  foreach($des_states as $states) { ?><div class="btn-gy"><a href="<?php echo url('taxonomy/term/'.$states->tid); ?>">English school in <?php echo $states->name; ?></a></div><?php } ?></p>
             </section>
            <?php } ?>
              </li>
          </ul>
      </div>
      </div>
    </div>
    <div>
      <a id="choose"></a>
      <div class="choose-section">
        <?php $block = module_invoke('block', 'block_view', 4); print $block['content']; ?>
      </div>
    </div>
    <div>
      <div class="contact-form" >
        <div class="container">
          <h2 class="page-section-title">Contact us <span>Please provide the following information and we will aim to respond within 48 hours: </span></h2>
          <div class="container-f">
            <?php
          $node = node_load('652');
          webform_node_view($node,'full');
          print theme_webform_view($node->content);
          ?>
          </div>
        </div>
      </div>
      <strong></strong> </div>
    <div class="course-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h3> <span>Other courses you might be interested in</span> </h3>
          </div>
        </div>
        <div class="box-container">
        <?php for($i = 0; $i< count($other_interest_course_img); $i++) { ?>
          <div class="box"> 
            <img src="<?php print file_create_url($other_interest_course_img[$i]['uri']); ?>" />
            <div class="content">
              <h4><?php print $other_interest_course_title[$i]; ?></h4>
              <p><?php print $other_interest_course_desc[$i]; ?></p>
              <div class="btn-gy">
                <a href="<?php print $other_interest_course_link[$i]; ?>">Find out more</a>
              </div>
            </div>
          </div>
        <?php } ?>
        </div>
      </div>
    </div>
    <div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content"> <a href="#" class="close" data-dismiss="modal" aria-label="Close"><span class="icon icon-ki-close"></span></a>
        <div class="modal-body">
          <form>
            <div class="search-row">
              <input placeholder="Search ..." class="form-control form-text" type="text" id="edit-term" name="term" value="" >
              <button class="icon icon-ki-search btn form-submit" type="submit" id="edit-submit--2" name="op" value=""></button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
<script src="/sites/all/themes/skytheme/js/jquery.bbq.js"></script>
<script src="/sites/all/themes/skytheme/js/jquery.atAccordionOrTabs.js"></script>

