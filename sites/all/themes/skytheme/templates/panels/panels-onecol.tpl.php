<?php

    $term = taxonomy_term_load(arg(2));
    $term_id = $term->tid;
    $node_wrapper = entity_metadata_wrapper('taxonomy_term', $term_id);
    $gallery_images = $node_wrapper->field_gallery_images->value();
    $main_body = $node_wrapper->field_main_body->value();
    ?>

    <div class="breadcrumb-wrapper">
      <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item"><a href="/english-courses">All English Courses</a></li>
        <li class="breadcrumb-iteme active">
          <span href="<?php print url('taxonomy/term/'.$term->tid); ?>"><?php print $term->name; ?></span>
        </li>
      </ol>
    </div>

    <div class="new-york-pg flex-eng-course">
      <div class="eng-course">
        <div class="container">
          <h2 class="page-header"><?php echo $node_wrapper->field_h1_title->value(); ?></h2>
          <div class="subline-tag"><?php echo $node_wrapper->field_subtitle->value(); ?></div>
        </div>
        <div class="tab-menus">
          <div class="container">
            <ul>
              <li><a class="js-scroll-trigger" href="#over-eng">Overview</a></li>
              <li><a class="js-scroll-trigger" href="#course-list">Course list</a></li>
            </ul>
          </div>
        </div>
      </div>
          
      <div class="container top-container">
        <div class="top-section">
          <div class="row-section">
            <div class="row">
              <div class="col-sm-6 col-1-dv">
                <div id="newyork-gallery" class="carousel slide" data-ride="carousel" data-interval="false">
                  <div class="carousel-inner">
                    <?php $i = 0; foreach($gallery_images as $gallery_image) { ?>
                    <div class="carousel-item <?php if($i == 0) { ?> active <?php } ?> ">
                      <img src="/sites/default/files/<?php echo $gallery_image['filename']; ?>" 
                        alt="<?php echo $gallery_image['alt']; ?>">
                    </div>
                    <?php $i++; } ?>
                  </div>
                  <!-- Left and right controls -->
                  <a class="carousel-control-prev control" href="#newyork-gallery" data-slide="prev">
                    <span class="icon icon-ki-arrow-left "></span>
                  </a>
                  <a class="carousel-control-next control" href="#newyork-gallery" data-slide="next">
                    <span class="icon icon-ki-arrow-right"></span>
                  </a>
                </div>
              </div>
              <div class="col-sm-6 col-2-dv">
                <div class="video-view">
                  <iframe width="560" height="100%" src="https://www.youtube.com/embed/L1shgss2eBo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div> 
              </div>   
            </div>
          </div>
        </div>

        <div class="learn-eng" id="over-eng">
          <div class="row">
            <div class="col-xs-12 col-sm-8 ">
              <div class="sec-left">
                <h3>English language courses to suit your needs</h3>
                <?php print $main_body['value']; ?>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4 ">
              <div class="sec-right">
                <div id="testimonial-gallery" class="carousel slide" data-ride="carousel1" data-interval="false">
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <div class="user-img">
                        <img src="/sites/all/themes/skytheme/images/student-testimonial-chicago-nouf-alkhalagi.jpg" width="75" height="75" />
                      </div>
                      <div class=" name-text">Nouf Alkhalagi<span>Studied in KI Chicago</span></div>
                      <div class="summry">
                        "Now I don't feel shy when I speak English. Moreover, I can tell jokes and stand in front of an audience and give a speech. My English skills will help me with my work. I'll try to speak in English even in my country. I want to learn more and more. Also, I can help people in the language like how the school did for me."
                      </div>
                    </div>
                    <div class="carousel-item">
                      <div class="user-img">
                        <img src="/sites/all/themes/skytheme/images/student-testimonial-chicago-nouf-alkhalagi.jpg" width="75" height="75" />
                      </div>
                      <div class=" name-text">Nouf Alkhalagi<span>Studied in KI Chicago</span></div>
                      <div class="summry">
                        "Now I don't feel shy when I speak English. Moreover, I can tell jokes and stand in front of an audience and give a speech. My English skills will help me with my work. I'll try to speak in English even in my country. I want to learn more and more. Also, I can help people in the language like how the school did for me."
                      </div>
                    </div>
                    <div class="carousel-item">
                    <div class="user-img"><img src="/sites/all/themes/skytheme/images/student-testimonial-chicago-nouf-alkhalagi.jpg" width="75" height="75" /></div>
                    <div class=" name-text">Nouf Alkhalagi<span>Studied in KI Chicago</span></div>
                    <div class="summry">
                    "Now I don't feel shy when I speak English. Moreover, I can tell jokes and stand in front of an audience and give a speech. My English skills will help me with my work. I'll try to speak in English even in my country. I want to learn more and more. Also, I can help people in the language like how the school did for me."

                    </div>
                    </div>
                    <div class="carousel-item">
                    <div class="user-img"><img src="/sites/all/themes/skytheme/images/student-testimonial-chicago-nouf-alkhalagi.jpg" width="75" height="75" /></div>
                    <div class=" name-text">Nouf Alkhalagi<span>Studied in KI Chicago</span></div>
                    <div class="summry">
                    "Now I don't feel shy when I speak English. Moreover, I can tell jokes and stand in front of an audience and give a speech. My English skills will help me with my work. I'll try to speak in English even in my country. I want to learn more and more. Also, I can help people in the language like how the school did for me."

                    </div>
                    </div>
                    <div class="carousel-item">
                    <div class="user-img"><img src="/sites/all/themes/skytheme/images/student-testimonial-chicago-nouf-alkhalagi.jpg" width="75" height="75" /></div>
                    <div class=" name-text">Nouf Alkhalagi<span>Studied in KI Chicago</span></div>
                    <div class="summry">
                    "Now I don't feel shy when I speak English. Moreover, I can tell jokes and stand in front of an audience and give a speech. My English skills will help me with my work. I'll try to speak in English even in my country. I want to learn more and more. Also, I can help people in the language like how the school did for me."

                    </div>
                    </div>
        <!--<div class="num"></div>-->
                  </div>
        <!-- Left and right controls -->
                  <div class="link-item">
                    <ul class="carousel-indicators">
                      <li data-target="#testimonial-gallery" data-slide-to="0" class="active"><span>1</span></li>
                      <li data-target="#testimonial-gallery" data-slide-to="1"><span>2</span></li>
                      <li data-target="#testimonial-gallery" data-slide-to="2"><span>3</span></li>
                      <li data-target="#testimonial-gallery" data-slide-to="3"><span>4</span></li>
                      <li data-target="#testimonial-gallery" data-slide-to="4"><span>5</span></li>
                      <li data-target="#testimonial-gallery" data-slide-to="5"><span>6</span></li>
                      <li data-target="#testimonial-gallery" data-slide-to="6"><span>7</span></li>
                      <li data-target="#testimonial-gallery" data-slide-to="7"><span>8</span></li>
                      <li data-target="#testimonial-gallery" data-slide-to="8"><span>9</span></li>
                      <li data-target="#testimonial-gallery" data-slide-to="9"><span>10</span></li>
                    </ul>
                    <span>to 10</span>
                    <a class="carousel-control-prev" href="#testimonial-gallery" data-slide="prev">
                      <span class="icon icon-ki-arrow-left"></span>
                    </a>
                    <a class="carousel-control-next" href="#testimonial-gallery" data-slide="next">
                      <span class="icon icon-ki-arrow-right"></span>
                    </a>
                  </div>
                </div> 
              </div> 
              <div class="free-block">
                <div class="test">
                  <a href="#">Free English Test</a>
                  A quick English test to check your grammar, listening and reading skills
                </div> 
              </div>  

              <div class="free-block">
                <div class="learn">
                  <a href="#">K+ Learning system</a>
                    K+ is Kaplan's exclusive English Language Learning System
                </div> 
              </div>           
            </div>
          </div>
        </div>  
      </div>
      <?php print views_embed_view('courses_available_under_top_vocab_term', 'default', $term_id); ?>

      <div class="field-row">
         <div class="container top-container">
             
               <div class="row">
               <div class="col-sm-4">
                  <div class="field-details">
                     <h3>Why should you choose Kaplan International?</h3>
                     <p>Learning English should be an adventure. Set in spectacular locations from palm-fringed beaches to the heart of New York, our schools offer you dynamic language tuition, sophisticated learning technology, and the amazing experience of exploring the English-speaking world. Whatever your ambitions, our immersive courses are designed to help you achieve your goals – and enrich your life.</p>
                     <a href="#" class="more-find">Find out more</a>
                     
                  </div>
               </div>
               <div class="col-sm-4">
                  <div class="field-details">
                     <h3>CERTIFIED PROGRESS</h3>
                     <p>It is important you have proof of your studies and the hard work you have put in.</p>
                     <p>At the end of your course you will receive a Kaplan International English Certificate of Achievement documenting your new language level and attendance. We are proud to be accredited by highly-respected and recognized accrediting agencies in each country.</p>
                     <a href="#" class="more-find">Find out more</a>
                     
                  </div>
               </div>
               <div class="col-sm-4">
                  <div class="field-details">
                     <h3>K+ English Language Learning System</h3>
                     <p>Our unique K+ teaching methodology was designed by an international team of experts to harness the power of digital, text-based and interpersonal learning methods. Our integrated system of books, apps, online resources and games work together to help you learn fast – in the classroom, on the move, and at home.</p>
                     <a href="#" class="more-find">Find out more</a>
                     
                  </div>
               </div>
             </div>
         </div>
      </div>
        <script src="/sites/all/themes/skytheme/js/jquery.easing.min.js.js"></script>
        <script src="/sites/all/themes/skytheme/js/scroll.js"></script>