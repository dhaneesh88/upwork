<?php
$term = taxonomy_term_load(arg(2));
$term_id = $term->tid;
?>
<div class="breadcrumb-wrapper">
  <nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="/">Home</a></li>
	    <li class="breadcrumb-item"><a href="/english-courses">All English Courses</a></li>
	    <li class="breadcrumb-iteme active">
	      <span href="<?php print url('taxonomy/term/'.$term->tid); ?>"><?php print $term->name; ?></span>
	    </li>
	  </ol>
</div>
<div class="new-york-pg us-course">
	<div class="container top-container">
		<div class="eng-school">
		<?php print views_embed_view('schools_at_this_destination', 'block_1', $term_id); ?>
		</div>
	</div>
</div>