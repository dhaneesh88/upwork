<div class="eng-school">
             <div class="container top-container" id="course-list">
               <div class="row">
                 <div class="col-sm-12"><h3>Course list</h3></div>
                 <div class="col-sm-12">
                    <div class="table-cont course-list">
                      <table class="table table-hover table-striped">  
                          <thead>
                                    <tr>
                                      <th>  Course  </th>
                                      <th>  Description </th>
                                      <th >  Course structure </th>
                                     <th > English level </th>
                                     <th >Where to Study</th>
                                  </tr>
                        </thead>
      <tbody>                
            <tr class="odd views-row-first">
        <!-- Views columns -->
                  <td class="full-wd-td"><a href="#" class="lnk ">Classic Program 40+</a>
                  <a href="#" class="more-link">Find out more</a> </td>
                  <td class="wd-label" data-label="Description">Learn, socialize and explore in a stimulating, grown-up atmosphere. With two unforgettable destinations to choose from, you can experience the romance and elegance of historic Bath or the natural beauty and seaside charm of Torquay. Either way you'll benefit from world-class language tuition in a beautiful, classically English setting. But the Kaplan experience doesn't end in the classroom. Roam atmospheric moorland, enjoy a traditional cream tea, learn about life in Roman and Georgian Britain, or take a boat trip down the River Avon.</td>
                  <td class="wd-label" data-label="Course structure">
<div class="para-text">
  <span class="td-hd">20 English lessons (15 hours)</span>
  <p>Our English language classes cover all the key areas of language and will help you master difficult grammatical structures, fine-tune your pronunciation, read complex texts and build a large working vocabulary.</p>
</div>

<div class="para-text">
  <span class="td-hd">3-5 weekday activities</span>
  <p>Weekday activities in Torquay include a tour of traditional English pubs and boat trips across the bay. In Bath they include visits to the Victoria Art Gallery and Jane Austen Centre.</p>
</div>

<div class="para-text">
  <span class="td-hd">1 Full day excursion</span>
  <p>In Torquay, explore the cathedral city of Exeter or the wild Dartmoor National Park. In Bath, roam around the ancient university town of Oxford and explore Shakespeare's beautiful birth town, Stratford-upon-Avon.</p>
</div>

<div class="para-text">
  <span class="td-hd">K+ Online and K+ Online Extra</span>
  <p>After your classes you will have access to our exclusive K+ Online materials, which help you reinforce the progress made in class and provide instant feedback</p>
</div>

</td>
                  <td class="wd-label"  data-label="English level"> 
                             Beginner to Advanced English
                  <p><a href="#" class="more-link">What is my indicative English level <span class="icon icon-ki-question"></span></a></p>
                   </td>
                  <td><div class="country-list">United Kingdom</div> </td>
               </tr>
               
                <tr class="even views-row-last">
                     <!-- Views columns -->
                  <td class="full-wd-td" ><a href="#" class="lnk">English for Academic Pathways</a>
                  <a href="#" class="more-link">Find out more</a></td>
                  <td class="wd-label" data-label="Description"> Develop your vocabulary and grammar, particularly in the key academic speaking skills of discussion and presentation. It allows you to gain direct entry into a range of universities, colleges and high schools in Canada</td>
                  <td class="wd-label" data-label="Course structure">
                  <div class="para-text">
  <span class="td-hd">20 English lessons (15 hours)</span>
  <p>Our English language classes cover all the key areas of language and will help you master difficult grammatical structures, fine-tune your pronunciation, read complex texts and build a large working vocabulary.</p>
</div>

<div class="para-text">
  <span class="td-hd">8 English for Academic Pathways Specific Skills lessons (6 hours) </span>
  <p>Focus on developing the core language skills and strategies necessary for success in an academic context. While improving your vocabulary and grammar you will practice tasks such as note-taking, presenting and essay planning to build your confidence.
</p>
</div>
                  
                  </td>
                  <td class="wd-label" data-label="English level">
                  Higher Intermediate or B2
                  <p><a href="#" class="more-link">What is my indicative English level <span class="icon icon-ki-question"></span></a></p></td>
                  
         <td>Canada</td>
               </tr>
               
               
               <tr class="even views-row-last row-3">
                     <!-- Views columns -->
                  <td class="full-wd-td" ><a href="#" class="lnk">General English course</a>
                  <a href="#" class="more-link">Find out more</a></td>
                  <td class="wd-label" data-label="Description"> Improve your English language skills to give you more professional and academic options in the future. As well as taking lessons, you will have plenty of time to sight-see and explore your destination</td>
                  <td class="wd-label" data-label="Course structure">
                  <div class="para-text">
  <span class="td-hd"> 20 General English lessons (15 hours)</span>
  <p>Our General English language classes cover all the key areas of language and will help you master difficult grammatical structures, fine-tune your pronunciation, read complex texts and build a large working vocabulary.</p>
</div> </td>
                  <td class="wd-label" data-label="English level">
                   Elementary to Advanced English
                  <p><a href="#" class="more-link">What is my indicative English level <span class="icon icon-ki-question"></span></a></p></td>
                  
         <td>United Kingdom, Ireland, New Zealand, Australia, Canada, United States</td>
               </tr>
               
               <tr class="even views-row-last row-4">
                     <!-- Views columns -->
                  <td class="full-wd-td" ><a href="#" class="lnk">Intensive English course</a>
                  <a href="#" class="more-link">Find out more</a></td>
                  <td class="wd-label" data-label="Description">A fast, enjoyable, and effective way to improve your English language skills, whatever your current language level. Customize your course with specific skills lessons, including business English and exam preparation</td>
                  <td class="wd-label" data-label="Course structure">
                  <div class="para-text">
  <span class="td-hd"> 20 English lessons (15 hours)</span>
  <p>Our English language classes cover all the key areas of language and will help you master difficult grammatical structures, fine-tune your pronunciation, read complex texts and build a large working vocabulary.</p>
</div>

  <div class="para-text">
  <span class="td-hd"> 8 Specific Skills Lessons (6 hours)</span>
  <p>Focus on a range of specific skills such as vocabulary, business English, or local culture.</p>
</div>

 <div class="para-text">
  <span class="td-hd"> 7 K+ sessions (5.25 hours)</span>
  <p>Our exclusive K+ Online and K+ Online Extra materials help you reinforce progress made in class and provide instant feedback, while our teacher-led K+ Learning Clubs allow you to socialize and practice your language skills at the same time. </p>
</div>

 <div class="para-text">
  <span class="td-hd">Optional Intensive Upgrade supplement</span>
  <p>If you want to maximize your study time you can choose to increase your weekly lessons with an Intensive Upgrade package of 8 lessons, (6 hours). In the UK we also have a Super Intensive package of 12 lessons available (8 hours). </p>
</div>

 </td>
                  <td class="wd-label" data-label="English level">
                   Elementary to Advanced English
                  <p><a href="#" class="more-link">What is my indicative English level <span class="icon icon-ki-question"></span></a></p></td>
                  
         <td>United Kingdom, Ireland, New Zealand, Australia, Canada, United States</td>
               </tr>
               
               <tr class="even views-row-last row-5">
                     <!-- Views columns -->
                  <td class="full-wd-td" ><a href="#" class="lnk">Intensive English Evening Class</a>
                  <a href="#" class="more-link">Find out more</a></td>
                  <td class="wd-label" data-label="Description">This course is ideal for students who are working during the day, but still need to maintain their student visa requirements by studying full time. Evening classes are typically from 4.15pm to 8.30pm, with a 15 minute break in between</td>
                  <td class="wd-label" data-label="Course structure">
                  <div class="para-text">
  <span class="td-hd"> 20 English lessons (15 hours)</span>
  <p>Our English language classes cover all the key areas of language and will help you master difficult grammatical structures, fine-tune your pronunciation, read complex texts and build a large working vocabulary.</p>
</div>

  <div class="para-text">
  <span class="td-hd"> 7 K+ sessions (5.25 hours) </span>
  <p>Our exclusive K+ Online and K+ Online Extra materials help you reinforce progress made in class and provide instant feedback, while our teacher-led K+ Learning Clubs allow you to socialize and practice your language skills at the same time.</p>
</div>
 </td>
                  <td class="wd-label" data-label="English level">
                   Elementary to Advanced English 
                  <p><a href="#" class="more-link">What is my indicative English level <span class="icon icon-ki-question"></span></a></p></td>
                  
         <td>Australia</td>
               </tr>
               
               <tr class="even views-row-last row-5">
                     <!-- Views columns -->
                  <td class="full-wd-td" ><a href="#" class="lnk">Semi-Intensive English course</a>
                  <a href="#" class="more-link">Find out more</a></td>
                  <td class="wd-label" data-label="Description">A semi-intensive programme that allows you to balance classroom tuition with using the Study Centre and free time activities. Improve your English language ability at your own speed, while having time to enjoy your destination</td>
                  <td class="wd-label" data-label="Course structure">
                  <div class="para-text">
  <span class="td-hd">20 Semi-Intensive English lessons (15 hours)</span>
  <p>Our Semi-Intensive English language classes cover all the key areas of language and will help you master difficult grammatical structures, fine-tune your pronunciation, read complex texts and build a large working vocabulary.</p>
</div>

  <div class="para-text">
  <span class="td-hd"> 7 K+ sessions (5.25 hours)</span>
  <p>Our exclusive K+ Online and K+ Online Extra materials help you reinforce progress made in class and provide instant feedback, while our teacher-led K+ Learning Clubs allow you to socialize and practice your language skills at the same time.</p>
</div>
 </td>
                  <td class="wd-label" data-label="English level">
                   Elementary to Advanced English 
                  <p><a href="#" class="more-link">What is my indicative English level <span class="icon icon-ki-question"></span></a></p></td>
                  
         <td>United Kingdom, Ireland, New Zealand, Canada, United States</td>
               </tr>
               
    
  </tbody>

</table>
                    </div>
                 </div>
               </div>
               
             </div>
           
           </div>


 <div class="field-row" >
         <div class="container top-container">
             
               <div class="row">
               <div class="col-sm-4">
                  <div class="field-details">
                     <h3>Why should you choose Kaplan International?</h3>
                     <p>Learning English should be an adventure. Set in spectacular locations from palm-fringed beaches to the heart of New York, our schools offer you dynamic language tuition, sophisticated learning technology, and the amazing experience of exploring the English-speaking world. Whatever your ambitions, our immersive courses are designed to help you achieve your goals – and enrich your life.</p>
                     <a href="#" class="more-find">Find out more</a>
                     
                  </div>
               </div>
               <div class="col-sm-4">
                  <div class="field-details">
                     <h3>CERTIFIED PROGRESS</h3>
                     <p>It is important you have proof of your studies and the hard work you have put in.</p>
                     <p>At the end of your course you will receive a Kaplan International English Certificate of Achievement documenting your new language level and attendance. We are proud to be accredited by highly-respected and recognized accrediting agencies in each country.</p>
                     <a href="#" class="more-find">Find out more</a>
                     
                  </div>
               </div>
               <div class="col-sm-4">
                  <div class="field-details">
                     <h3>K+ English Language Learning System</h3>
                     <p>Our unique K+ teaching methodology was designed by an international team of experts to harness the power of digital, text-based and interpersonal learning methods. Our integrated system of books, apps, online resources and games work together to help you learn fast – in the classroom, on the move, and at home.</p>
                     <a href="#" class="more-find">Find out more</a>
                     
                  </div>
               </div>
             </div>
         </div>
      </div>