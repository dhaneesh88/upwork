<?php
/**
*
*/
?>
<div class="breadcrumb-wrapper">
  <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">All English Courses</a></li>
    <li class="breadcrumb-iteme"><a href="#">English for ages 16+</a></li>
    <li class="breadcrumb-iteme active"><a href="#">Flexible English language courses</a></li>
  </ol>
</div>
<div class="new-york-pg flex-eng-course">
	<div class="eng-course">
        <div class="container">
           <h2 class="page-header">Flexible English Language Courses</h2>
           <div class="subline-tag">Customize your learning experience by choosing the length and intensity of your course</div>
		</div>
    	<div class="tab-menus">
       		<div class="container">
	            <ul >
	              <li><a class="js-scroll-trigger" href="#over-eng">Overview</a></li>
	              <li><a class="js-scroll-trigger" href="#course-list">Course list</a></li>
            	</ul>
          	</div>
    	</div>
	</div>
	<div class="container top-container">
		<div class="top-section">
			<div class="row-section">
				<div class="row">
					<div class="col-sm-6 col-1-dv">
                		<div id="newyork-gallery" class="carousel slide" data-ride="carousel" data-interval="false">
							<div class="carousel-inner">
								<div class="carousel-item active">
								  <img src="images/gallery/eng-ad-flex-01_0.jpg" alt="Los Angeles">
								</div>
								<div class="carousel-item">
								  <img src="images/gallery/eng-ad-flex-03_0.jpg" alt="Chicago">
								</div>
								<div class="carousel-item">
								  <img src="images/gallery/eng-ad-flex-03_0.jpg" alt="New York">
								</div>
							</div>
  							<!-- Left and right controls -->
							<a class="carousel-control-prev control" href="#newyork-gallery" data-slide="prev">
							<span class="icon icon-ki-arrow-left "></span>
							</a>
							<a class="carousel-control-next control" href="#newyork-gallery" data-slide="next">
							<span class="icon icon-ki-arrow-right"></span>
							</a>
						</div>
					</div>
					<div class="col-sm-6 col-2-dv">
						<div class="video-view">
						<iframe width="560" height="100%" src="https://www.youtube.com/embed/L1shgss2eBo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
						</div> 
					</div>
				</div>
			</div>
		</div>