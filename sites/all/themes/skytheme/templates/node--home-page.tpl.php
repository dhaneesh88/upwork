<?php

?>
<div class="banner-section-top">
  <div class="inner-banner">
    <div class="large-right">
      <div class="bg-img full-bg">
        <div class="bg-img-light full-bg">
          <div class="ks-logo"><img src="<?php print 'sites/all/themes/skytheme/'.theme_get_setting('hero_logo'); ?>"/></div>
          <div class="banner-text">
            <h2><?php print theme_get_setting('hero_text'); ?></h2>
            <div class="tag-line"><?php print theme_get_setting('hero_subtitle'); ?></div>
            <div class="button-section">
              <div class="btn-blue">
                <a class="btn btn-1 " href="hero_button_1_url"><?php print theme_get_setting('hero_button_1_text'); ?></a>
              </div>
              <div class="btn-blue">
                <a class="btn btn-2 " href="hero_button_2_url"><?php print theme_get_setting('hero_button_2_text'); ?></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="img-cols right-banner">
      <div class="img-col-1 course">
        <a href="<?php print theme_get_setting('right_box_1_url'); ?>" class="full-bg">
             <span class="full-block full-bg" >
               <span class="text-h-m ">
                  <span class="text-h"><?php print theme_get_setting('right_box_1_caption'); ?></span>
                 <span class="text-more"><?php print theme_get_setting('right_box_1_roll_text'); ?> →</span>
               </span>
             </span>
        </a>
      </div>
      <div class="img-col-1 all pos-rel ">
        <a href="<?php print theme_get_setting('right_box_2_url'); ?>" class="full-bg">
             <span class="full-block full-bg" >
               <span class="text-h-m ">
                  <span class="text-h"><?php print theme_get_setting('right_box_2_caption'); ?></span>
               <span class="text-more"><?php print theme_get_setting('right_box_2_roll_text'); ?> →</span>
             </span>
           </span>
        </a>
        <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item item active one-img full-bg"></div>
            <div class="carousel-item item two-img full-bg"></div>
            <div class="carousel-item item three-img full-bg"></div>
          </div>
        </div>
      </div>


      <div class="img-col-1">
        <a href="<?php print theme_get_setting('right_box_3_url'); ?>" class="full-bg">
             <span class="full-block full-bg" >
               <span class="text-h-m ">
                  <span class="text-h"><?php print theme_get_setting('right_box_3_caption'); ?></span>
                  <span class="text-more"><?php print theme_get_setting('right_box_3_roll_text'); ?> →</span>
              </span>
             </span>
        </a>
      </div>

    </div>
  </div>

</div>

<div class="in-section-2">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2> <?php print theme_get_setting('homepage_h2'); ?>
          <span><?php print theme_get_setting('homepage_subtitle'); ?></span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-3 col-xs-6">
        <img src="<?php print 'sites/all/themes/skytheme/'.theme_get_setting('home_box_1_icon'); ?>" />
        <p><?php print theme_get_setting('home_box_1_message'); ?></p>
      </div>

      <div class="col-sm-3 col-xs-6">
        <img src="<?php print 'sites/all/themes/skytheme/'.theme_get_setting('home_box_2_icon'); ?>" />
        <p><?php print theme_get_setting('home_box_2_message'); ?></p>
      </div>

      <div class="col-sm-3 col-xs-6">
        <img src="<?php print 'sites/all/themes/skytheme/'.theme_get_setting('home_box_3_icon'); ?>" />
        <p><?php print theme_get_setting('home_box_3_message'); ?></p>
      </div>

      <div class="col-sm-3 col-xs-6">
        <img src="<?php print 'sites/all/themes/skytheme/'.theme_get_setting('home_box_4_icon'); ?>" />
        <p><?php print theme_get_setting('home_box_4_message'); ?></p>
      </div>
    </div>
  </div>
</div>

<div class="in-section-3">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3> <?php print theme_get_setting('third_section_h3'); ?>
          <span><?php print theme_get_setting('third_section_subtitle'); ?></span></h3>
      </div>

    </div>
    <div class="img-cols section-row">
      <div class="img-col-1 cols img-1">
        <a href="<?php print theme_get_setting('third_section_box_1_url'); ?>" class="full-bg">
           <span class="full-block full-bg" >
             <span class="text-h-m ">
              <span class="arrow"></span>
              <span class="text-h"><?php print theme_get_setting('third_section_box_1_caption'); ?></span>
              <span class="text-more"><?php print theme_get_setting('third_section_box_1_roll_text'); ?> →</span>
             </span>
           </span>
        </a>
      </div>
      <div class="img-col-1 cols img-2">
        <a href="<?php print theme_get_setting('third_section_box_2_url'); ?>" class="full-bg">
             <span class="full-block full-bg" >
                <span class="text-h-m ">
                  <span class="arrow"></span>
                  <span class="text-h"><?php print theme_get_setting('third_section_box_2_caption'); ?></span>
                  <span class="text-more"><?php print theme_get_setting('third_section_box_1_roll_text'); ?> →</span>
                </span>
             </span>
        </a>
      </div>
      <div class="img-col-1 cols img-3">
        <a href="<?php print theme_get_setting('third_section_box_3_url'); ?>" class="full-bg">
             <span class="full-block full-bg" >
                <span class="text-h-m ">
                  <span class="arrow"></span>
                  <span class="text-h"><?php print theme_get_setting('third_section_box_3_caption'); ?></span>
                  <span class="text-more"><?php print theme_get_setting('third_section_box_1_roll_text'); ?> →</span>
                </span>
             </span>
        </a>
      </div>

      <div class="img-col-1 cols img-4">
        <a href="<?php print theme_get_setting('third_section_box_4_url'); ?>" class="full-bg">
             <span class="full-block full-bg" >
                <span class="text-h-m ">
                  <span class="arrow"></span>
                  <span class="text-h"><?php print theme_get_setting('third_section_box_4_caption'); ?></span>
                  <span class="text-more"><?php print theme_get_setting('third_section_box_1_roll_text'); ?> →</span>
                </span>
             </span>
        </a>
      </div>

      <div class="img-col-1 cols img-5">
        <a href="<?php print theme_get_setting('third_section_box_5_url'); ?>" class="full-bg">
             <span class="full-block full-bg" >
                <span class="text-h-m ">
                <span class="arrow"></span>
                <span class="text-h"><?php print theme_get_setting('third_section_box_5_caption'); ?></span>
                <span class="text-more"><?php print theme_get_setting('third_section_box_1_roll_text'); ?> →</span>
              </span>
             </span>
        </a>
      </div>
    </div>
    <div class="col-md-12">
      <div class="text-center">
        <a class="blue-btn"
            href="<?php print theme_get_setting('third_section_button_url'); ?>">
            <?php print theme_get_setting('third_section_button_text'); ?> →</a>
      </div>
    </div>
  </div>
</div>

<div class="in-section-4">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3> <?php print theme_get_setting('fourth_section_h3'); ?></h3>
      </div>
    </div>

    <div class="img-cols section-row">
      <div class="img-col-1 cols img-1">
        <a href="<?php print theme_get_setting('fourth_section_box_1_url'); ?>" class="full-bg">
             <span class="full-block full-bg" >
               <span class="text-h-m ">
               <span class="arrow"></span>
                  <span class="text-h"><?php print theme_get_setting('fourth_section_box_1_caption'); ?></span>
                 <span class="text-more"><?php print theme_get_setting('fourth_section_box_1_roll_text'); ?> →</span>
               </span>
               </span>
             </span>
        </a>
      </div>
      <div class="img-col-1 wd-50 cols img-2">
        <a href="<?php print theme_get_setting('fourth_section_box_2_url'); ?>" class="full-bg">
               <span class="full-block full-bg" >
               <span class="text-h-m ">
               <span class="arrow"></span>
                  <span class="text-h"><?php print theme_get_setting('fourth_section_box_2_caption'); ?></span>
               <span class="text-more"><?php print theme_get_setting('fourth_section_box_2_roll_text'); ?> →</span>
               </span>
               </span>
        </a>
      </div>
      <div class="img-col-1 cols img-3">
        <a href="<?php print theme_get_setting('fourth_section_box_3_url'); ?>" class="full-bg">
               <span class="full-block full-bg" >
               <span class="text-h-m ">
               <span class="arrow"></span>
                  <span class="text-h"><?php print theme_get_setting('fourth_section_box_3_caption'); ?></span>
               <span class="text-more"><?php print theme_get_setting('fourth_section_box_3_roll_text'); ?> →</span>
               </span>
               </span>
        </a>
      </div>

      <div class="img-col-1 wd-50 cols img-4">
        <a href="<?php print theme_get_setting('fourth_section_box_4_url'); ?>" class="full-bg">
               <span class="full-block full-bg" >
               <span class="text-h-m ">
               <span class="arrow"></span>
                  <span class="text-h"><?php print theme_get_setting('fourth_section_box_4_caption'); ?></span>
               <span class="text-more"><?php print theme_get_setting('fourth_section_box_4_roll_text'); ?> →</span>
               </span>
               </span>
        </a>
      </div>

      <div class="img-col-1 cols img-5">
        <a href="<?php print theme_get_setting('fourth_section_box_5_url'); ?>" class="full-bg">
               <span class="full-block full-bg" >
               <span class="text-h-m ">
               <span class="arrow"></span>
                  <span class="text-h"><?php print theme_get_setting('fourth_section_box_5_caption'); ?></span>
               <span class="text-more"><?php print theme_get_setting('fourth_section_box_5_roll_text'); ?> →</span></span>
               </span>
        </a>
      </div>
      <div class="img-col-1 cols img-6">
        <a href="<?php print theme_get_setting('fourth_section_box_6_url'); ?>" class="full-bg">
               <span class="full-block full-bg" >
               <span class="text-h-m ">
               <span class="arrow"></span>
                  <span class="text-h"><?php print theme_get_setting('fourth_section_box_6_caption'); ?></span>
               <span class="text-more"><?php print theme_get_setting('fourth_section_box_6_roll_text'); ?> →</span>
               </span>
               </span>
        </a>
      </div>
    </div>
    <div class="col-md-12">
      <div class="text-center">
        <a class="blue-btn" href="<?php print theme_get_setting('fourth_section_button_url'); ?>"><?php print theme_get_setting('fourth_section_button_text'); ?> →</a>
      </div>
    </div>
  </div>
</div>
