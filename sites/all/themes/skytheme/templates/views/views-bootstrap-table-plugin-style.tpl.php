<?php

/**
 * @file
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $caption: The caption for this table. May be empty.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * - $responsive: A flag indicating whether table is responsive.
 * @ingroup views_templates
 */

$title_array = array();
?>
<div class="col-sm-12">
<?php if (!empty($title)): ?>
  <h3><?php print $title ?></h3>
<?php endif ?>
</div>
<div class="col-sm-12">
  <div class="table-cont course-list">
    <table class="table table-hover table-striped">
      <?php if (!empty($title) || !empty($caption)) : ?>
        <caption><?php print $caption . $title; ?></caption>
      <?php endif; ?>
      <?php if (!empty($header)) : ?>
        <thead>
        <tr>
          <?php foreach ($header as $field => $label): ?>
            <th <?php if ($header_classes[$field]) { print 'class="'. $header_classes[$field] . '" '; } ?>>
              <?php $title_array[] = $label; print $label; ?>
            </th>
          <?php endforeach; ?>
        </tr>
        </thead>
      <?php endif; ?>
      <tbody>
      <?php $i = 0; foreach ($rows as $row_count => $row): ?>
        <tr <?php if($i == 0) { ?> class="odd views-row-first <?php print 'row-'.$row_count; ?>" <?php } else { ?> class="even views-row-last <?php print 'row-'.$row_count; ?>" <?php } ?>>
          <?php $j = 0; foreach ($row as $field => $content): ?>
            <td <?php if($j == 0) { ?> class="full-wd-td" <?php } else { ?> class="wd-label" <?php } ?> data-label="<?php print $title_array[$j]; ?>">
              <?php print $content; ?>
            </td>
          <?php $j++; endforeach; ?>
        </tr>
      <?php $i++; endforeach; ?>
      </tbody>
    </table>
  </div>
</div>