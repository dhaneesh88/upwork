<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div class="carousel-item <?php if($id == 0) { ?>active<?php } ?>">
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
<!-- Left and right controls -->
  <a class="carousel-control-prev control" href="#newyork-gallery" data-slide="prev">
    <span class="icon icon-ki-arrow-left "></span>
  </a>
  <a class="carousel-control-next control" href="#newyork-gallery" data-slide="next">
    <span class="icon icon-ki-arrow-right"></span>
  </a>