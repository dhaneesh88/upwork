<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php foreach ($rows as $id => $row): ?>
  <div class="carousel-item <?php if($id == 0) { ?>active<?php } ?>">
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
	<!-- Left and right controls -->
 	<div class="link-item">
 		<ul class="carousel-indicators">
	<?php foreach ($rows as $id => $row): ?>
		<li data-target="#testimonial-gallery" data-slide-to="<?php print $id; ?>" <?php if($id == 0) { ?>class="active"<?php } ?>>
			<span><?php print $id + 1; ?></span>
		</li>
	<?php endforeach; ?>
	</ul>
		<span>to 10</span>
        <a class="carousel-control-prev" href="#testimonial-gallery" data-slide="prev">
            <span class="icon icon-ki-arrow-left"></span>
        </a>
        <a class="carousel-control-next" href="#testimonial-gallery" data-slide="next">
            <span class="icon icon-ki-arrow-right"></span>
        </a>
    </div>