<?php
/**
 * Replacement for theme_form_element().
 *
 * Override.
 * Changes: just return $element['#children'];
 */
function skytheme_webform_element($variables) {

  $element = $variables['element'];
  $wrapper_attributes_class['class'] = $element['#wrapper_attributes']['class'][0];
  $output = '<div ' . drupal_attributes($wrapper_attributes_class) . '>' . "\n";
  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . webform_filter_xss($element['#field_prefix']) . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . webform_filter_xss($element['#field_suffix']) . '</span>' : '';

  // Generate description for above or below the field.
  $above = !empty($element['#webform_component']['extra']['description_above']);
  $description = array(
    FALSE => '',
    TRUE => !empty($element['#description']) ? ' <div class="description">' . $element['#description'] . "</div>\n" : '',
  );

  // If #children does not contain an element with a matching @id, do not
  // include @for in the label.
  if (isset($variables['element']['#id']) && strpos($element['#children'], ' id="' . $variables['element']['#id'] . '"') === FALSE) {
    $variables['element']['#id'] = NULL;
  }

  switch ($element['#title_display']) {
    case 'inline':
      $output .= $description[$above];
      $description[$above] = '';
    case 'before':
    case 'invisible':
    case 'after':
      $title = ' ' . theme('form_element_label', $variables);
      break;
  }

  $children = ' ' . $description[$above] . $prefix . $element['#children'] . $suffix;
  switch ($element['#title_display']) {
    case 'inline':
    case 'before':
    case 'invisible':
      $output .= $title;
      $output .= $children;
      break;

    case 'after':
      $output .= $children;
      $output .= $title;
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= $children;
      break;
  }
  $output .= "\n";

  $output .= $description[!$above];
  $output .= "</div>\n";

  return $output;
}

///**
// * Replacement for theme_form_element().
// *
// * Override.
// * Changes: just return $element['#children'];
// */
//function skytheme_webform_element($variables) {
//
//}

function skytheme_theme_select($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id', 'name', 'size'));
  _form_set_class($element, array('form-select'));

  return '<select' . drupal_attributes($element['#attributes']) . '>' . form_select_options($element) . '</select>';
}