<?php

function ukb_form_system_theme_settings_alter(&$form, &$form_state) {
	

    $logotop = theme_get_setting('logotop');
    if (file_uri_scheme($logotop) == 'public') {
        $logotop = file_uri_target($logotop);
    }
    $logomobtop = theme_get_setting('logomobtop');
    if (file_uri_scheme($logomobtop) == 'public') {
        $logomobtop = file_uri_target($logomobtop);
    }
    $logobottom = theme_get_setting('logobottom');
    if (file_uri_scheme($logobottom) == 'public') {
        $logobottom = file_uri_target($logobottom);
    }
    $banner_blog = theme_get_setting('banner_blog');
    if (file_uri_scheme($banner_blog) == 'public') {
        $banner_blog = file_uri_target($banner_blog);
    }
    $banner_coll = theme_get_setting('banner_coll');
    if (file_uri_scheme($banner_coll) == 'public') {
        $banner_coll = file_uri_target($banner_coll);
    }
    $adv1 = theme_get_setting('adv1');
    if (file_uri_scheme($adv1) == 'public') {
        $adv1 = file_uri_target($adv1);
    }
    $adv2 = theme_get_setting('adv2');
    if (file_uri_scheme($adv2) == 'public') {
        $adv2 = file_uri_target($adv2);
    }


    $form['options'] = array(
        '#type' => 'vertical_tabs',
        '#default_tab' => 'defaults',
        '#weight' => '-10',
    );
    $form['options']['default'] = array(
        '#type' => 'fieldset',
        '#title' => 'Default',
    );
    $form['options']['default']['theme_settings'] = $form['theme_settings'];
    unset($form['theme_settings']);
    //$form['options']['default']['logo'] = $form['logo'];
    unset($form['logo']);
    $form['options']['default']['favicon'] = $form['favicon'];
    unset($form['favicon']);
    $form['options']['general'] = array(
        '#type' => 'fieldset',
        '#title' => 'Main',
    );
    $form['options']['general']['name'] = array(
        '#type' => 'textfield',
        '#title' => 'Name site for links ',
        '#default_value' => theme_get_setting('name'),
    );
    $form['options']['general']['twitter'] = array(
        '#type' => 'textfield',
        '#title' => 'Twitter ',
        '#default_value' => theme_get_setting('twitter'),
    );
    $form['options']['general']['facebook'] = array(
        '#type' => 'textfield',
        '#title' => 'Facebook ',
        '#default_value' => theme_get_setting('facebook'),
    );
    
    
    
     $form['options']['general']['benbook'] = array(
        '#type' => 'textfield',
        '#title' => 'Benbook ',
        '#default_value' => theme_get_setting('benbook'),
    );
    
    
    $form['options']['general']['youtube'] = array(
        '#type' => 'textfield',
        '#title' => 'Youtube ',
        '#default_value' => theme_get_setting('youtube'),
    );
    $form['options']['general']['pinterest'] = array(
        '#type' => 'textfield',
        '#title' => 'Pinterest ',
        '#default_value' => theme_get_setting('pinterest'),
    );
    $form['options']['general']['instagram'] = array(
        '#type' => 'textfield',
        '#title' => 'Instagram ',
        '#default_value' => theme_get_setting('instagram'),
    );
    $form['options']['general']['promo'] = array(
        '#type' => 'textfield',
        '#title' => 'Promo ',
        '#default_value' => theme_get_setting('promo'),
    );
    $form['options']['general']['phone'] = array(
        '#type' => 'textfield',
        '#title' => 'Phone ',
        '#default_value' => theme_get_setting('phone'),
    );
    $form['options']['general']['footer'] = array(
        '#type' => 'textfield',
        '#title' => 'Footer content ',
        '#default_value' => theme_get_setting('footer'),
    );
    $form['options']['general']['copy'] = array(
        '#type' => 'textfield',
        '#title' => 'Copy inf ',
        '#default_value' => theme_get_setting('copy'),
    );
    $form['options']['general']['block_logotop'] = array(
        '#type' => 'fieldset',
    );
    $form['options']['general']['block_logotop']['logotop'] = array(
        '#type' => 'textfield',
        '#title'=> 'Logo top',
        '#default_value' => $logotop,
    );
    $form['options']['general']['block_logotop']['logotop_upload'] = array(
        '#type' => 'file',
        '#title' => 'Upload logo top',
    );
    $form['options']['general']['block_logomobtop'] = array(
        '#type' => 'fieldset',
    );
    $form['options']['general']['block_logomobtop']['logomobtop'] = array(
        '#type' => 'textfield',
        '#title'=> 'Logo mob top',
        '#default_value' => $logomobtop,
    );
    $form['options']['general']['block_logomobtop']['logomobtop_upload'] = array(
        '#type' => 'file',
        '#title' => 'Upload logo mob top',
    );
    $form['options']['general']['block_logobottom'] = array(
        '#type' => 'fieldset',
    );
    $form['options']['general']['block_logobottom']['logobottom'] = array(
        '#type' => 'textfield',
        '#title'=> 'Logo bottom',
        '#default_value' => $logobottom,
    );
    $form['options']['general']['block_logobottom']['logobottom_upload'] = array(
        '#type' => 'file',
        '#title' => 'Upload logo bottom',
    );
   
    $form['options']['menu'] = array(
        '#type' => 'fieldset',
        '#title' => 'Main menu',
    );
    $form['options']['menu']['block_adv1'] = array(
        '#type' => 'fieldset',
    );
    $form['options']['menu']['block_adv1']['adv1'] = array(
        '#type' => 'textfield',
        '#title'=> 'Ad 1 collumn',
        '#default_value' => $adv1,
    );
    $form['options']['menu']['block_adv1']['adv1_upload'] = array(
        '#type' => 'file',
        '#title' => 'Upload Ad 1',
    );
    $form['options']['menu']['field_1'] = array(
        '#type' => 'textfield',
        '#title' => 'Subtitle ',
        '#default_value' => theme_get_setting('field_1'),
    );
    $form['options']['menu']['block_adv2'] = array(
        '#type' => 'fieldset',
    );
    $form['options']['menu']['block_adv2']['adv2'] = array(
        '#type' => 'textfield',
        '#title'=> 'Ad 2 collumn',
        '#default_value' => $adv2,
    );
    $form['options']['menu']['block_adv2']['adv2_upload'] = array(
        '#type' => 'file',
        '#title' => 'Upload Ad 2',
    );
    $form['options']['menu']['field_2'] = array(
        '#type' => 'textfield',
        '#title' => 'Subtitle ',
        '#default_value' => theme_get_setting('field_2'),
    );

    $form['options']['blog'] = array(
        '#type' => 'fieldset',
        '#title' => 'Blog',
    );
    $form['options']['blog']['name_blog'] = array(
        '#type' => 'textfield',
        '#title' => 'Name blog ',
        '#default_value' => theme_get_setting('name_blog'),
    );
    $form['options']['blog']['sidebar_blog'] = array(
        '#type' => 'textarea',
        '#title' => 'Sidebar text blog ',
        '#default_value' => theme_get_setting('sidebar_blog'),
    );
    $form['options']['blog']['block_banner_blog'] = array(
        '#type' => 'fieldset',
    );
    $form['options']['blog']['block_banner_blog']['banner_blog'] = array(
        '#type' => 'textfield',
        '#title'=> 'Blog Banner',
        '#default_value' => $banner_blog,
    );
    $form['options']['blog']['block_banner_blog']['banner_blog_upload'] = array(
        '#type' => 'file',
        '#title' => 'Upload blog banner',
    );

    $form['options']['coll'] = array(
        '#type' => 'fieldset',
        '#title' => 'Collection',
    );
    $form['options']['coll']['name_coll'] = array(
        '#type' => 'textfield',
        '#title' => 'Name collection ',
        '#default_value' => theme_get_setting('name_coll'),
    );
    $form['options']['coll']['sidebar_coll'] = array(
        '#type' => 'textarea',
        '#title' => 'Sidebar text collection ',
        '#default_value' => theme_get_setting('sidebar_coll'),
    );
    $form['options']['coll']['block_banner_coll'] = array(
        '#type' => 'fieldset',
    );
    $form['options']['coll']['block_banner_coll']['banner_coll'] = array(
        '#type' => 'textfield',
        '#title'=> 'Collection Banner',
        '#default_value' => $banner_coll,
    );
    $form['options']['coll']['block_banner_coll']['banner_coll_upload'] = array(
        '#type' => 'file',
        '#title' => 'Upload collection banner',
    );
    
    $form['options']['newsletter_setings'] = array(
        '#type' => 'fieldset',
        '#title' => 'Newsletter mail settings',
    );    

    $form['options']['newsletter_setings']['mail'] = array(
        '#type' => 'textfield',
        '#title' => 'E-Mail to receive the subscription',
        '#default_value' => theme_get_setting('mail'),
    );
    $form['options']['newsletter_setings']['mail_from'] = array(
        '#type' => 'textfield',
        '#title' => 'E-Mail from which the answer is to subscribers ',
        '#default_value' => theme_get_setting('mail_from'),
    );

    $form['options']['newsletter_first'] = array(
        '#type' => 'fieldset',
        '#title' => 'Newsletter first form',
    );
    $form['options']['newsletter_first']['nlf_text_form'] = array(
        '#type' => 'textarea',
        '#title' => 'Text form ',
        '#default_value' => theme_get_setting('nlf_text_form'),
    );
    $form['options']['newsletter_first']['nlf_subject'] = array(
        '#type' => 'textfield',
        '#title' => 'Subject ',
        '#default_value' => theme_get_setting('nlf_subject'),
    );
    $form['options']['newsletter_first']['nlf_text_body'] = array(
        '#type' => 'textarea',
        '#title' => 'Body ',
        '#default_value' => theme_get_setting('nlf_text_body'),
    );
    
    $form['options']['notify_product'] = array(
        '#type' => 'fieldset',
        '#title' => 'Notify product form',
    );
    $form['options']['notify_product']['np_subject'] = array(
        '#type' => 'textfield',
        '#title' => 'Subject ',
        '#default_value' => theme_get_setting('np_subject'),
    );
    $form['options']['notify_product']['np_text_body'] = array(
        '#type' => 'textarea',
        '#title' => 'Body ',
        '#default_value' => theme_get_setting('np_text_body'),
    );

    $form['options']['newsletter_blog'] = array(
        '#type' => 'fieldset',
        '#title' => 'Newsletter blog form',
    );
    $form['options']['newsletter_blog']['nlb_text_form'] = array(
        '#type' => 'textarea',
        '#title' => 'Text form ',
        '#default_value' => theme_get_setting('nlb_text_form'),
    );
    $form['options']['newsletter_blog']['nlb_subject'] = array(
        '#type' => 'textfield',
        '#title' => 'Subject ',
        '#default_value' => theme_get_setting('nlb_subject'),
    );
    $form['options']['newsletter_blog']['nlb_text_body'] = array(
        '#type' => 'textarea',
        '#title' => 'Body ',
        '#default_value' => theme_get_setting('nlb_text_body'),
    );

    $form['options']['newsletter_all'] = array(
        '#type' => 'fieldset',
        '#title' => 'Newsletter all pages form',
    );
    $form['options']['newsletter_all']['nlall_text_form'] = array(
        '#type' => 'textfield',
        '#title' => 'Text form ',
        '#default_value' => theme_get_setting('nlall_text_form'),
    );
    $form['options']['newsletter_all']['nlall_subject'] = array(
        '#type' => 'textfield',
        '#title' => 'Subject ',
        '#default_value' => theme_get_setting('nlall_subject'),
    );
    $form['options']['newsletter_all']['nlall_text_body'] = array(
        '#type' => 'textarea',
        '#title' => 'Body ',
        '#default_value' => theme_get_setting('nlall_text_body'),
    );

    $form['options']['contact'] = array(
        '#type' => 'fieldset',
        '#title' => 'Contact form',
    );
    $form['options']['contact']['contact_text_form'] = array(
        '#type' => 'textfield',
        '#title' => 'Text form ',
        '#default_value' => theme_get_setting('contact_text_form'),
    );
    $form['options']['contact']['contact_subject'] = array(
        '#type' => 'textfield',
        '#title' => 'Subject ',
        '#default_value' => theme_get_setting('contact_subject'),
    );
    $form['options']['contact']['contact_text_body'] = array(
        '#type' => 'textarea',
        '#title' => 'Body ',
        '#default_value' => theme_get_setting('contact_text_body'),
    );

    $form['options']['payment'] = array(
        '#type' => 'fieldset',
        '#title' => 'Payment service icons',
    );
    $form['options']['payment']['am_express'] = array(
        '#type' => 'checkbox',
        '#title' => 'American express ',
        '#default_value' => theme_get_setting('am_express'),
    );
    $form['options']['payment']['apple_pay'] = array(
        '#type' => 'checkbox',
        '#title' => 'Apple pay ',
        '#default_value' => theme_get_setting('apple_pay'),
    );
    $form['options']['payment']['din_club'] = array(
        '#type' => 'checkbox',
        '#title' => 'Diners club ',
        '#default_value' => theme_get_setting('din_club'),
    );
    $form['options']['payment']['discover'] = array(
        '#type' => 'checkbox',
        '#title' => 'Discover ',
        '#default_value' => theme_get_setting('discover'),
    );
    $form['options']['payment']['goo_pay'] = array(
        '#type' => 'checkbox',
        '#title' => 'Google pay ',
        '#default_value' => theme_get_setting('goo_pay'),
    );
    $form['options']['payment']['jcb'] = array(
        '#type' => 'checkbox',
        '#title' => 'Jcb ',
        '#default_value' => theme_get_setting('jcb'),
    );
    $form['options']['payment']['master'] = array(
        '#type' => 'checkbox',
        '#title' => 'Master ',
        '#default_value' => theme_get_setting('master'),
    );
    $form['options']['payment']['paypal'] = array(
        '#type' => 'checkbox',
        '#title' => 'PayPal ',
        '#default_value' => theme_get_setting('paypal'),
    );
    $form['options']['payment']['ukb_pay'] = array(
        '#type' => 'checkbox',
        '#title' => 'ukb pay ',
        '#default_value' => theme_get_setting('ukb_pay'),
    );
    $form['options']['payment']['visa'] = array(
        '#type' => 'checkbox',
        '#title' => 'Visa ',
        '#default_value' => theme_get_setting('visa'),
    );

    $form['#submit'][] = 'ukb_settings_submit';
}

function ukb_settings_submit($form, &$form_state) {    

    $image_1 = 'public://' . $form['options']['general']['block_logotop']['logotop']['#default_value'];
    $file_1 = file_save_upload('logotop_upload');
    if ($file_1) {
        $_1 = pathinfo($file_1->filename);
        $destination_1 = 'public://' . $_1['basename'];
        $file_1->status = FILE_STATUS_PERMANENT;
        if (file_copy($file_1, $destination_1, FILE_EXISTS_REPLACE)) {
            $_POST['logotop'] = $form_state['values']['logotop'] = $destination_1;
            if ($destination_1 != $image_1) {
                return;
            }
        }
    } else {
        $_POST['logotop'] = $form_state['values']['logotop'] = $image_1;
    }
    $image_2 = 'public://' . $form['options']['general']['block_logomobtop']['logomobtop']['#default_value'];
    $file_2 = file_save_upload('logomobtop_upload');
    if ($file_2) {
        $parts_2 = pathinfo($file_2->filename);
        $destination_2 = 'public://' . $parts_2['basename'];
        $file_2->status = FILE_STATUS_PERMANENT;
        if (file_copy($file_2, $destination_2, FILE_EXISTS_REPLACE)) {
            $_POST['logomobtop'] = $form_state['values']['logomobtop'] = $destination_2;
            if ($destination_2 != $image_2) {
                return;
            }
        }
    } else {
        $_POST['logomobtop'] = $form_state['values']['logomobtop'] = $image_2;
    }
    $image_3 = 'public://' . $form['options']['general']['block_logobottom']['logobottom']['#default_value'];
    $file_3 = file_save_upload('logobottom_upload');
    if ($file_3) {
        $parts_3 = pathinfo($file_3->filename);
        $destination_3 = 'public://' . $parts_3['basename'];
        $file_3->status = FILE_STATUS_PERMANENT;
        if (file_copy($file_3, $destination_3, FILE_EXISTS_REPLACE)) {
            $_POST['logobottom'] = $form_state['values']['logobottom'] = $destination_3;
            if ($destination_3 != $image_3) {
                return;
            }
        }
    } else {
        $_POST['logobottom'] = $form_state['values']['logobottom'] = $image_3;
    }
  
    $image_5 = 'public://' . $form['options']['blog']['block_banner_blog']['banner_blog']['#default_value'];
    $file_5 = file_save_upload('banner_blog_upload');
    if ($file_5) {
        $parts_5 = pathinfo($file_5->filename);
        $destination_5 = 'public://' . $parts_5['basename'];
        $file_5->status = FILE_STATUS_PERMANENT;
        if (file_copy($file_5, $destination_5, FILE_EXISTS_REPLACE)) {
            $_POST['banner_blog'] = $form_state['values']['banner_blog'] = $destination_5;
            if ($destination_5 != $image_5) {
                return;
            }
        }
    } else {
        $_POST['banner_blog'] = $form_state['values']['banner_blog'] = $image_5;
    } 
    $image_6 = 'public://' . $form['options']['coll']['block_banner_coll']['banner_coll']['#default_value'];
    $file_6 = file_save_upload('banner_coll_upload');
    if ($file_6) {
        $parts_6 = pathinfo($file_6->filename);
        $destination_6 = 'public://' . $parts_6['basename'];
        $file_6->status = FILE_STATUS_PERMANENT;
        if (file_copy($file_6, $destination_6, FILE_EXISTS_REPLACE)) {
            $_POST['banner_coll'] = $form_state['values']['banner_coll'] = $destination_6;
            if ($destination_6 != $image_6) {
                return;
            }
        }
    } else {
        $_POST['banner_coll'] = $form_state['values']['banner_coll'] = $image_6;
    }
    $image_7 = 'public://' . $form['options']['menu']['block_adv1']['adv1']['#default_value'];
    $file_7 = file_save_upload('adv1_upload');
    if ($file_7) {
        $parts_7 = pathinfo($file_7->filename);
        $destination_7 = 'public://' . $parts_7['basename'];
        $file_7->status = FILE_STATUS_PERMANENT;
        if (file_copy($file_7, $destination_7, FILE_EXISTS_REPLACE)) {
            $_POST['adv1'] = $form_state['values']['adv1'] = $destination_7;
            if ($destination_7 != $image_7) {
                return;
            }
        }
    } else {
        $_POST['adv1'] = $form_state['values']['adv1'] = $image_7;
    } 
    $image_8 = 'public://' . $form['options']['menu']['block_adv2']['adv2']['#default_value'];
    $file_8 = file_save_upload('adv2_upload');
    if ($file_8) {
        $parts_8 = pathinfo($file_8->filename);
        $destination_8 = 'public://' . $parts_8['basename'];
        $file_8->status = FILE_STATUS_PERMANENT;
        if (file_copy($file_8, $destination_8, FILE_EXISTS_REPLACE)) {
            $_POST['adv2'] = $form_state['values']['adv2'] = $destination_8;
            if ($destination_8 != $image_8) {
                return;
            }
        }
    } else {
        $_POST['adv2'] = $form_state['values']['adv2'] = $image_8;
    } 
}