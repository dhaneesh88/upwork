<?php

/**
 * @file
 * The primary PHP file for the Drupal ukb base theme.
 *
 * This file should only contain light helper functions and point to stubs in
 * other files containing more complex functions.
 *
 * The stubs should point to files within the `./includes` folder named after
 * the function itself minus the theme prefix. If the stub contains a group of
 * functions, then please organize them so they are related in some way and name
 * the file appropriately to at least hint at what it contains.
 *
 * All [pre]process functions, theme functions and template files lives inside
 * the `./templates` folder. This is a highly automated and complex system
 * designed to only load the necessary files when a given theme hook is invoked.
 *
 * Visit this project's official documentation site https://drupal-ukb.org
 * or the markdown files inside the `./docs` folder.
 *
 * @see _ukb_theme()
 */

function ukb_theme_preprocess_page(&$vars) {
  if (isset($vars['node']->type)) {
    $vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type;
  }
}

/**
 * @see _ukb_theme()
 */
function ukb_theme(&$existing, $type, $theme, $path) {
  ukb_include($theme, 'includes/registry.inc');
  return _ukb_theme($existing, $type, $theme, $path);
}

/**
 * Clear any previously set element_info() static cache.
 *
 * If element_info() was invoked before the theme was fully initialized, this
 * can cause the theme's alter hook to not be invoked.
 *
 * @see https://www.drupal.org/node/2351731
 */
drupal_static_reset('element_info');

/**
 * Declare various hook_*_alter() hooks.
 *
 * All hook_*_alter() implementations must live (via include) inside this file
 * so they are properly detected when drupal_alter() is invoked.
 */
//ukb_include('ukb', 'includes/alter.inc');


