(function ($) {
  Drupal.TBNex = Drupal.TBNex || {};
  
  Drupal.TBNex.initContactForm = function(){
    $('#edit-submitted-contact-name').placeholder({value:'Name*'});
    $('#edit-submitted-contact-email').placeholder({value: 'Email*'});
    $('#edit-submitted-contact-subject').placeholder({value: 'Subject*'});
    $('#edit-submitted-contact-phone').placeholder({value: 'Phone*'});
    $('#edit-submitted-contact-message').placeholder({value: 'Message'});
  }
  
  Drupal.behaviors.actionTBNex = {
    attach: function (context) {
      $('.btn-backtotop').smoothScroll({
        speed: 350
      });
      $('.view-rows-2').each(function(){
        $(this).find('.grid-inner').matchHeights();
      });
      $('#comments .comment').filter(':last').addClass('last');
      Drupal.TBNex.initContactForm();
    }
  };
})(jQuery);
