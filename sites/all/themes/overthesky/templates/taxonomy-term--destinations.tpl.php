<div class="row">
  <div class="destination_slider col-sm-6"><?php print views_embed_view('destinations_slider', 'block', $term->tid); ?></div>
  <div class="destination_video col-sm-6"><?php print render($content['field_video']); ?></div>
</div>
<div class="row">
  <div class="col-sm-8">
    <h1 class="title"><?php print render($content['field_h1_title']); ?></h1>
    <h2 class="title"><?php print render($content['field_h2_title']); ?></h2>
    <div class="description"><?php print render($content['description']); ?></div>
  </div>
  <div class="col-sm-4 destination-testimonials"><?php print views_embed_view('destination_testimonials', 'block_3', $term->tid); ?></div>
</div>

<div class="eng-school map-wrapper" >
  <div class="container top-container">
    <div class="row">
      <div class="col-sm-12" id="s-location">
        <h3>Destinations in United States</h3>
        <div class="ny-map"> <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d48382.07172340574!2d-74.03394390804617!3d40.72066948573628!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2599b210c1209%3A0xf8a6386be89ee3c5!2sKaplan+International+English+-+New+York+SoHo!5e0!3m2!1sen!2sin!4v1540293056344" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe></div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-12 destination-schools">
    <?php print views_embed_view('schools_at_this_destination', 'block', $term->tid); ?>
  </div>
</div>

<?php
//print "<pre>"; print_r($content); print "</pre>"; die;
?>
