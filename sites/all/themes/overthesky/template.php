<?php

/**
 * @file
 * The primary PHP file for this theme.
 */

function overthesky_preprocess_page(&$variables) {
  if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
    $tid = arg(2);
    $vocabulary = db_query("SELECT machine_name FROM {taxonomy_vocabulary} LEFT JOIN {taxonomy_term_data} ON taxonomy_term_data.vid = taxonomy_vocabulary.vid WHERE tid = :tid", array(':tid' => $tid))->fetchField();
    $variables['theme_hook_suggestions'][] = 'page__vocabulary__' . $vocabulary;
    
    if($vocabulary == 'destinations') {
      //$variables['page']['content']['system_main']['#access'] = FALSE;
      
      //unset($variables['page']['content']['system_main']['term_heading']);
      unset($variables['page']['content']['system_main']['nodes']);
      unset($variables['page']['content']['system_main']['no_content']);
      unset($variables['page']['content']['system_main']['pager']);
      //print "<pre>"; print_r($variables['page']['content']['system_main']); print "</pre>"; die;
    }
  }
}
