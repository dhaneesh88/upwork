(function ($) {

Drupal.behaviors.webform_ajax = {

  attach: function (context, settings) {

    // Fully declare element_settings, as Drupal's ajax.js seems not to merge defaults correctly.
    $('.messages', context).each(function () {
      $wrapper = $(this).parents('[id^=webform-ajax-wrapper]');
      if ($wrapper.length) {
          var topval = $('.messages').offset().top;
          $('html, body').animate({
          scrollTop: topval
          }, 1000);
       }
    });
  }

};

}(jQuery));
