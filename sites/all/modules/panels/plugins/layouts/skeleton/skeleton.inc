<?php

/**
 * @file
 * Implementation of hook_panels_layouts().
 */
// Plugin definition.
$plugin = array(
  'title' => t('Skeleton'),
  'category' => t('Columns: 1'),
  'icon' => 'skeleton.png',
  'theme' => 'panels_skeleton',
  'css' => 'skeleton.css',
  'regions' => array('middle' => t('Middle column')),
);
