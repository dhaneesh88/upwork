(function ($) {
    Drupal.behaviors.webform_ajax = {
        attach: function (context, settings) {
            function s() {
                $(".dropajs").slideUp();
            }
            function t() {
                $(".thqdropajs").slideUp();
            }
            $("#brokernotestool")[0].reset();
                $("input[type=\"checkbox\"]").click(function() {
                $(this).is(":checked") ? (0 == $(this).closest("ul li").index() && 0 == $("#dropin_Penny_Stocks").length && $(".style_add #dropin_Futures_Trading").parent().after("<li><input class=\"anChk\" name=\"style_drop\" id=\"dropin_Penny_Stocks\" value=\"Penny Stocks\" type=\"radio\"><label for=\"dropin_Penny_Stocks\" class=\"anCheck_class upperA\"><div class=\"chkBx\"></div>Penny Stocks</label></li>"), 1 == $(this).closest("ul li").index() && 0 == $("#dropin_forex").length && $(".style_add #dropin_Spread_Betting").parent().after("<li><input class=\"anChk\" name=\"style_drop\" id=\"dropin_forex\" value=\"Forex Trading\" type=\"radio\"><label for=\"dropin_forex\" class=\"anCheck_class upperA\"><div class=\"chkBx\"></div>Forex Trading</label></li>"), 3 == $(this).closest("ul li").index() && 0 == $("#dropin_Investing").length && $("ul.style_add").append("<li><input class=\"anChk\" name=\"style_drop\" id=\"dropin_Investing\" value=\"Investing\" type=\"radio\"><label for=\"dropin_Investing\" class=\"anCheck_class upperA\"><div class=\"chkBx\"></div>Investing</label></li>")) : $(this).is(":not(:checked)") && (0 == $(this).closest("ul li").index() && $("#dropin_Penny_Stocks").parent().remov$(), 1 == $(this).closest("ul li").index() && $("#dropin_Penny_Stocks").parent().remov$(), 3 == $(this).closest("ul li").index() && $("#dropin_Investing").parent().remov$())
            })
            $("ul.style_add li").on("click", function() {
                if ($("#tradingLabel").text($(this).find("label").text()), $("input[name=style_drop]:checked").length > 0 && $("input[name=ans2]:checked").length > 0)
                    if (1 == $("input[name=ans2]:checked").val()) {
                        if (0 == $(".anChk").is(":checked")) {
                            var s = parseFloat($(".i_value").val()) + 1;
                            $(".rsltpglnk").removeClass("dsblit"), $(".ovrTxt").hide(), $(".shrtMsg").show(), $(".i_value").val(s)
                        } else 1 == $(this).is(":checked") || ($(".rsltpglnk").removeClass("dsblit"), $(".ovrTxt").hide(), $(".shrtMsg").show());
                    } else 1 == $(".drop_down_2").is(":checked") && 1 == $(".anChk").is(":checked") ? ($(".rsltpglnk").removeClass("dsblit"), $(".ovrTxt").hide(), $(".shrtMsg").show()) : ($(".rsltpglnk").addClass("dsblit"), $(".shrtMsg").hide());
                ($("input[name=style_drop]:checked").length <= 0 || $("input[name=ans2]:checked").length <= 0) && (0 == $(".anChk").is(":checked") ? ($(".rsltpglnk").addClass("dsblit"), $(".shrtMsg").hide()) : 0 == $(".ansRdo").is(":checked") ? 0 == $(".ansRdo").is(":checked") ? ($(".rsltpglnk").addClass("dsblit"), $(".shrtMsg").hide()) : $(".rsltpglnk").removeClass("dsblit") : (0 == $(".anChk").is(":checked") || $(".drop_down_2").is(":checked"), $(".rsltpglnk").addClass("dsblit"), $(".shrtMsg").hide()))
            })
            $("input.ansRdo").click(function() {
                1 == $("input[name=ans2]:checked").val() ? $(".qutionC").hide() : $(".qutionC").show(), s(), t(), $("input[name=style_drop]:checked").length > 0 && $("input[name=ans2]:checked").length > 0 && (1 == $("input[name=ans2]:checked").val() ? ($(".rsltpglnk").removeClass("dsblit"), $(".ovrTxt").hide(), $(".shrtMsg").show()) : 1 == $(".drop_down_2").is(":checked") && 1 == $(".ansRdo").is(":checked") ? ($(".rsltpglnk").removeClass("dsblit"), $(".ovrTxt").hide(), $(".shrtMsg").show()) : ($(".rsltpglnk").addClass("dsblit"), $(".shrtMsg").hide())), ($("input[name=style_drop]:checked").length <= 0 || $("input[name=ans2]:checked").length <= 0) && ($(".rsltpglnk").addClass("dsblit"), $(".shrtMsg").hide())
            })
            $("input.drop_down_2").change(function() {
                if ($("input[name=style_drop]:checked").length > 0 && $("input[name=drop_down_2]:checked").length > 0)
                    if (1 == $("input[name=ans2]:checked").val()) {
                        if (0 == $(".anChk").is(":checked")) {
                            var s = parseFloat($(".i_value").val()) + 1;
                            $(".rsltpglnk").removeClass("dsblit"), $(".ovrTxt").hide(), $(".shrtMsg").show(), $(".i_value").val(s)
                        } else 1 == $(this).is(":checked") || ($(".rsltpglnk").removeClass("dsblit"), $(".ovrTxt").hide(), $(".shrtMsg").show());
                    } else 1 == $(".drop_down_2").is(":checked") && 1 == $(".anChk").is(":checked") ? ($(".rsltpglnk").removeClass("dsblit"), $(".ovrTxt").hide(), $(".shrtMsg").show()) : ($(".rsltpglnk").addClass("dsblit"), $(".shrtMsg").hide());
                $("input[name=style_drop]:checked").length <= 0 || $("input[name=drop_down_2]:checked").length
            })
            $(".trdName").unbind().click(function() {
                $(this).parent().find(".inrdio").click()
            })
            $(".icoHldr").unbind().click(function() {
                $(this).find(".inrdio").is(":checked") && $(this).closest(".stepBB") ? $(this).closest(".stepBB").find("a.grnBtnwide").show() : $(this).closest(".stepBB").find("a.grnBtnwide").hide()
            })
            $(".see_suggestion").click(function() {
                var s = [];
                $("input[name=trade]:checked").each(function() {
                    s.push($(this).attr("data-instrument"))
                }), void 0, void 0;
                var t = [];
                $("input[name=deposita]:checked").each(function() {
                    t.push($(this).val())
                }), void 0, void 0;
                var n = [];
                return $("input[name=ffeatures]:checked").each(function() {
                    n.push($(this).val())
                }), void 0, void 0, $("#brokernotestool").submit(), !1
            })
            $("input.inrdio").unbind().click(function() {
                $("input.inrdio:checked").length <= 0 ? ($(".unchkjs").fadeIn(), $(".chkjs").hide(), $(".maskIt").hide()) : ($(".unchkjs").hide(), $(".chkjs").fadeIn(), $(".maskIt").show())
            })
            $(document).ready(function() {}), $(window).resize(function() {}), $(".grnBtn").click(function() {
                var s = $(".whtpoptbx").width() - 2 * $(".whtpoptbx").width();
                return $(".stepAA").css("margin-left", s), $(".stepAA").css("opacity", "0"), $(".whtpoptbx").css("height", "auto"), window.scrollTo(0, 100), setTimeout(function() {
                    $(".stepAA").css("overflow", "hidden"), $(".whtpoptbx").css("overflow", "visible"), $(".overflowbx").css("width", "100%"), $(".stepAA").css("height", "10px"), $(".stepAA").css("width", "0px"), $(".stepAA").css("margin", "0px")
                }, 1e3), $(".stepBB").css("opacity", "100"), $(".stepBB").css("display", "inline"), !1
            })
            $(".grnBtnwide").click(function() {
                $("#depmax").is(":checked") && $("#fc").prop("checked", !0);
                var s = $(".whtpoptbx").width() - 2 * $(".whtpoptbx").width();
                return $(".stepAA").css("margin-left", s), $(".stepAA").css("opacity", "0"), $(".stepBB").css("margin-left", s), $(".stepBB").css("opacity", "0"), $(".whtpoptbx").css("height", "auto"), window.scrollTo(0, 100), setTimeout(function() {
                    $(".stepAA").css("overflow", "hidden"), $(".whtpoptbx").css("overflow", "visible"), $(".overflowbx").css("width", "100%"), $(".stepAA").css("height", "10px"), $(".stepAA").css("width", "0px"), $(".stepAA").css("margin", "0px"), $(".stepBB").css("overflow", "hidden"), $(".whtpoptbx").css("overflow", "visible"), $(".overflowbx").css("width", "100%"), $(".stepBB").css("height", "10px"), $(".stepBB").css("width", "0px"), $(".stepBB").css("margin", "0px")
                }, 1e3), $(".stepCC").css("opacity", "100"), $(".stepCC").css("display", "inline"), !1
            })
            $(".opendropajs").click(function() {
                return $(".dropajs").slideToggl$(), t(), !1
            })
            $(".thqopndropajs").click(function() {
                return $(".thqdropajs").slideToggl$(), s(), !1
            })
            $(window).on("load", function() {
                $("body").on("click", ".dropList.features ul li", function() {
                    $("#featuresLabel").text($(this).find("label").text())
                }), $("body").on("click", ".ubermenu-responsive-toggle-main", function() {
                    $("#mobile-menu").toggl$()
                })
            })
        }
    };

}(jQuery));

"use strict";
jQuery(document).ready(function() {
    var e = function e() {
        var e, i, t, u, s, r, n, c;
        jQuery("input#aa").is(":checked") && (e = 1), jQuery("input#bb").is(":checked") && (e = 2), jQuery("input#cc").is(":checked") && (e = 3), jQuery("input#dd").is(":checked") && (e = 4), jQuery("input#deposita").is(":checked") && (i = 1), jQuery("input#depositb").is(":checked") && (i = 2), jQuery("input#depositc").is(":checked") && (i = 3), jQuery("input#depmax").is(":checked") && (i = 4), t = jQuery("input#fa").is(":checked") ? "yes" : "no", u = jQuery("input#fb").is(":checked") ? "yes" : "no", s = jQuery("input#fc").is(":checked") ? "yes" : "no", r = jQuery("input#fd").is(":checked") ? "yes" : "no", n = jQuery("input#fe").is(":checked") ? "yes" : "no", c = jQuery("input#df").is(":checked") ? "yes" : "no", jQuery.ajax({
            type: "POST",
            url: "/count_api.php",
            dataType: "json",
            async: "false",
            data: {
                trade: e,
                deposit: i,
                offers_social_trading: t,
                islamic_account: u,
                offers_mt4: s,
                allows_scalping: r,
                allows_hedging: n,
                regulated: c,
                excludedcountries: 'IN'
            },
            beforeSend: function beforeSend() {
                jQuery("#loading-features").show()
            },
            success: function success() {
                jQuery("#loading-features").hide()
            },
            complete: function complete() {
                jQuery("#loading-features").hide()
            }
        }).done(function(e) {
            jQuery.each(e, function(e, i) {
                var t;
                t = i > 10 ? 10 : i, jQuery("#result-no").html(t);
                jQuery(".cflabel").html(["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"][t]), jQuery("#loading-features").hide()
            })
        }).fail(function() {})
    };
    
    jQuery(".stepBB input.inrdio").click(function() {
        e(), 0 == jQuery("#loading-features").children().length && jQuery("#loading-features").append("<span><img src=\"/images/loader.gif\" border=0 alt=\"Loading Image\" /></span>")
    }), jQuery("ul#featureoptions li").off("click"), jQuery("ul#featureoptions li").click(function() {
        setTimeout(function() {
            e()
        }, 500)
    }), jQuery(".uimob .stepAA .iconArea .inrdio").click(function() {
        setTimeout(function() {
            return jQuery(".stepAA").animate({
                "margin-left": "-3000px"
            }, "fast"), jQuery(".whtpoptbx").css("height", "auto"), setTimeout(function() {
                jQuery(".stepAA").css("overflow", "hidden"), jQuery(".whtpoptbx").css("overflow", "visible"), jQuery(".overflowbx").css("width", "100%"), jQuery(".stepAA").css("height", "10px"), jQuery(".stepAA").css("width", "0px"), jQuery(".stepAA").css("margin", "0px")
            }, 1e3), jQuery(".stepBB").css("opacity", "100"), jQuery(".stepBB").css("display", "inline"), !1
        }, 100)
    }), jQuery(".uimob .stepBB .iconArea .inrdio").click(function() {
        setTimeout(function() {
            jQuery("#bb").is(":checked") && jQuery("#depmax").is(":checked") && jQuery("#fc").prop("checked", !0);
            var e = jQuery(".whtpoptbx").width() - 2 * jQuery(".whtpoptbx").width();
            jQuery(".stepAA").css("margin-left", e), jQuery(".stepAA").css("opacity", "0"), jQuery(".stepBB").animate({
                "margin-left": "-3000px"
            }, "slow"), jQuery(".whtpoptbx").css("height", "auto"), setTimeout(function() {
                jQuery(".stepAA").css("overflow", "hidden"), jQuery(".whtpoptbx").css("overflow", "visible"), jQuery(".overflowbx").css("width", "100%"), jQuery(".stepBB").css("overflow", "hidden"), jQuery(".whtpoptbx").css("overflow", "visible"), jQuery(".overflowbx").css("width", "100%")
            }, 1e3), jQuery(".stepCC").css("opacity", "100"), jQuery(".stepCC").css("display", "inline")
        }, 400)
    })
});

